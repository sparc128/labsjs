/*
* ES6
*
* Лабораторная работа #8.
* Название: Array.
* Цели:
* 1. Научиться Использовать методы массивов
*    map, sort, filter, reduce, forEach языка JavaScript
* 2. Научиться работать с методами массивов
*    map, sort, filter, reduce, forEach языка JavaScript
*    в консоли браузера
* 2. Понять метод
* 3. Понять метод
* 3. Понять метод
* 4. Понять метод
* 5. Понять метод
* 6. Сформулировать выводы, записать exel файлом, прислать в лк и почту.
* Ход работы:
* 1. Открыть codepen.io
* 2. Ввести листинг кода представленный ниже.
* 3. Получить результаты
* 4. Сформулировать, записать выводы
* 5. Прислать на проверку в лк, и почту.
*    * Данные вопросы задают на собеседованиях.
* Отчет:
* 1. Написать цели работы
* 2. Написать что студент делал в ходе выполнения работы своими словами
* 3. Написать вывод о проделанной работе
* 4. *Если работа содержит листинг кода - написать листинг кода
* 5. *Если работа содержит рисунки - добавить рисунки в отчёт
* 6. *Если работа содержит вывод в Console браузера, Elements, Sources,
*     Вывод информации в окно браузера иными словами любые результаты
*     работы стандарта html5** то их надо вносить в отчет в свободной форме
*     **html5 - стандарт вклюает в себя html4 и ниже, CSS3 и ниже, ES6 и ниже
* Array
* */

const presidents = [
    {first: 'George', last: 'Washington', born: 1732, died: 1799},
    {first: 'John', last: 'Adams', born: 1735, died: 1826},
    {first: 'Ronald', last: 'Reagan', born: 1911, died: 2004},
    {first: 'Gerals', last: 'Ford', born: 1913, died: 2006},
    {first: 'Richard', last: 'Nixon', born: 1913, died: 1994},
    {first: 'John F.', last: 'Kennedy', born: 1917, died: 1963},
    {first: 'Harry S.', last: 'Truman', born: 1884, died: 1972},
    {first: 'Grover', last: 'Cleveland', born: 1837, died: 1908},
    {first: 'Chester A.', last: 'Arthur', born: 1829, died: 1886},
    {first: 'Abraham', last: 'Linkoln', born: 1809, died: 1865},
    {first: 'Franklin', last: 'Pierce', born: 1804, died: 1869},
    {first: 'Lyndon B.', last: 'Johnson', born: 1908, died: 1973},
    {first: 'Dwight D.', last: 'Eisenhower', born: 1890, died: 1969},
];

const writers = ['Василий,Жуковский','Александр,Грибоедов','Александр,Пушкин',
    'Владимир,Даль','Николай,Языков','Фёдор,Тютчев','Николай,Гоголь',
    'Алексей,Кольцов','Александр,Герцен','Александр,Радищев',
    'Иван,Гончаров','Михаил,Лермонтов','Пётр,Ершов',
    'Алексей,Толстой','Даниил,Заточник','Евгений,Баратынский',
    'Пётр,Вяземский','Александр,Бестужев-Марлинский','Михаил,Загоскин',
    'Сергей,Аксаков','Владимир,Одоевский','Григорий,Данилевский',
    'Алексей,Писемский','Дмитрий,Григорович','Яков,Полонский',
    'Леонид,Андреев','Валерий,Брюсов','Саша,Черный','Юрий,Трифонов',
    'Фёдор,Абрамов','Дмитрий,Кедрин','Василий,Шукшин'];

// Array.prototype.filter()
// 1. Отфильтровать президентов которые родились в 1700-х годах.
const born1700 = presidents.filter(president => president.born >= 1700 && president.born < 1800);
// console.table(born1700)

// Array.prototype.map()
// 2. создать массив, который содержит только имя и фамилию каждого президента
const firstLast = presidents.map(president => `${president.first}, ${president.last}`);
// выводит имя и фамилию каждого президента в строчном формате
// console.log(firstLast)

// Array.prototype.sort()
// 3. Отсортировать президентов по году рождения - от старшего к младшему
// Вариант записи 1
const oldest1 = presidents.sort((a, b) => {
    if (a.born > b.born) {
        return 1;
    } else {
        return -1;
    }
});
// console.table(oldest1)

// Вариант записи 2
const oldest2 = presidents.sort((a, b) => {
    return `${a.born > b.born ? 1 : -1}`
});
// console.table(oldest2)

// Вариант записи 3 - т.к. это стрелочная фия записываем в одну строчку
const oldest3 = presidents.sort((a, b) => `${a.born > b.born ? 1 : -1}`);
// console.table(oldest3)

// Array.prototype.reduce()
// 4. Подсчитать общее количество лет жизни всех президентов
// Вариант 1
// для total надо всегда задавать начальное значение, зададим нуль 0
const totalLived1 = presidents.reduce((total, president) => {
    return total + (president.died - president.born);
}, 0); // значение total = 0
// console.log(totalLived1)

// Вариант 2
const totalLived2 = presidents.reduce((total, president) => total + (president.died - president.born), 0);
// console.log(totalLived2)

// 5. Отсортировать президентов в зависимости от количества прожитых лет.
// (от большего к меньшему)
// Вариант 1
const presidentsSorted1 = presidents.sort((a , b) => {
   const aLived = a.died - a.born;
   const bLived = b.died - b.born;
   if (aLived > bLived) {
       return -1;
   } else {
       return 1;
   }
});
// console.table(presidentsSorted1)

// Вариант 2
const presidentsSorted2 = presidents.sort((a , b) => {
    const aLived = a.died - a.born;
    const bLived = b.died - b.born;
    return `${aLived > bLived ? -1 : 1}`;
});
// console.table(presidentsSorted2)

// 6. Создать список названий городов (city) и областей (Country) Калифорнии содержащих "San"
// Вариант 1
// const table = document.querySelector('.sortable');
// const links = Array.from(table.querySelector('tbody').querySelectorAll('a'));
// console.log(links)
//
// const names = links.map(link => {
//     return link.textContent;
// });
// console.log(names)

// Вставим этот код в консоль конкретной странцы списка городов США на
// википедии и увидим что он дает ошибку на методе map потому что
// метод map не функция - метод map не работает для нашей переменной links
// это происходит потому что список наших тегов "a" это список а не массив
// и к NodeList мы не можем применять методы которые мы можем использовать
// с массивами это map, sort, reduce ...
// поэтому наш большой список ссылок надо перевести в массив
// как это сделать?
// const links = Array.from(table.querySelector('tbody').querySelectorAll('a'));
// Снова вставляем код в браузер получаем 634 названий городов США
// Теперь надо среди этих 634 городов надо получить те которые
// имеют приставку san
// const names1 = links.map(link => {
//     return link.textContent;
// }); // сюда добавляем по цепочке методы которые хотим использовать c каждым промежуточным результатом

// Результат
// const table = document.querySelector('.sortable');
// const links = Array.from(table.querySelector('tbody').querySelectorAll('a'));
//
// const names = links.map(link => {
//     return link.textContent;
// }).filter(city => {
//     return city.includes('San');
// })
// console.log(names)

// Вариант 2
// const tableT1 = document.querySelector('.sortable');
// const linksS = Array.from(tableT1.querySelector('tbody').querySelectorAll('a'));
// console.log(linksS);
// const namesS = linksS.map(link => link.textContent).filter(city => city.includes('А'));
// console.log(namesS);
// ** Для того чтобы это все заработало необходимо загрузить html файл из редактора
// затем в консоли произвести очистки и перезагрузить браузер на странице с википедией

// 7. Упражнение по сортировке
// Отсортировать массим "writers" по фамилии в алфавитном порядке
const writersName = writers.sort((writerA, writerB) => {
    const [firstA, lastA] = writerA.split(","); // для первого писателя получаем имя и фамилию в две разные переменные
    const [firstB, lastB] = writerB.split(","); // для второго писателя получаем имя и фамилию в две разные переменные
    // console.log(firstA); // список имен писателей
    // return `${lastA > lastB ? 1 : -1}`; // сортируем писателей по имени и фамилии - имя и фамилия нумерются в алфавитном порядке от А до Я
});
// console.table(writersName)
// для каждой переменной writerA, writerB мы можем использовать метод split
// и одновременно получить раздельные значения - отдельно имя - отдельно фамилию
// в отдельной переменной используя деструктурирование
// синтаксис деструктурирования const [firstA, lastA]

// 8. Упражнение по использованию метода Reduce
// Подсчитать сколько раз встречается каждый элемент в массиве
const data = ['truck', 'car', 'car', 'truck', 'bike', 'walk', 'car', 'van', 'bike', 'walk', 'car', 'van', 'car', 'truck'];

const dataCount = data.reduce((objCount, transport) => {
 if (!objCount[transport]) {
     objCount[transport] = 0;
 }
 objCount[transport]++;
 return objCount; // возвращаем финальный объект
}, {});
console.log(dataCount);
// надо обязательно указывать как выглядит наш объект objCount в методе reduce
// делаем это так - }, {})
// Механика - если в объекте , {}) - переменная objCount не содержит уже этот элемент [transport]
// а первым текущим элементом не забываем был 'truck' - то мы этот элемент в нее добавляем и присваиваем
// его значение нуль objCount[transport] = 0;
// потом бежим далее и указываем следующее условие

// 9. Упражнение по использованию методов .forEach, .push и Object.keys();
// Создать массив который будет содержать только уникальные значения данного массива:
const fruits = ['apples','bananas','oranges','apples','grapes','bananas','peaches','strawberries','oranges','apricots','bananas'];

const uniqueFruits = {};
fruits.forEach((fruit) => {
    uniqueFruits[fruit] = true;
}) // ничего не возвращает а что то делает с каждым элементом по которому бежит
console.log(uniqueFruits);
//получаем ключи объекта из нашего массива с уникальными элементами
console.log(Object.keys(uniqueFruits));












