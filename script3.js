/*
*  JavaScript размеры прокрутка и
*   координаты элементов на странице
*  и окна браузера
* */
// тестовый вывод
console.log("Hi");

/* Размеры окна браузера
*  clientWidth и clientHeight
*
*  доступная ширина и высота окна
* */

// тут мы фактически обращаемся к тегу html
const mainElement = document.documentElement;
const mainElementWidth = mainElement.clientWidth;
const mainElementHeight = mainElement.clientHeight;
console.log(mainElementWidth);
console.log(mainElementHeight);

// Ширина и высота окна браузера с полосами прокрутки
const windowWidth = window.innerWidth;
const windowHeight = window.innerHeight;
console.log(windowWidth);
console.log(windowHeight);

// Ширина и высота документа
// включая прокрученную часть
let scrollWidth = Math.max(
    document.body.scrollWidth, document.documentElement.scrollWidth,
    document.body.offsetWidth, document.documentElement.offsetWidth,
    document.body.clientWidth, document.documentElement.clientWidth,
);
let scrollHeight = Math.max(
    document.body.scrollHeight, document.documentElement.scrollHeight,
    document.body.offsetHeight, document.documentElement.offsetHeight,
    document.body.clientHeight, document.documentElement.clientHeight,
);
console.log(scrollWidth);
console.log(scrollHeight);

// получить количество прокрученных пикселей
// только для чтения
const windowScrollTop = window.pageYOffset;
const windowScrollLeft = window.pageXOffset;
console.log(windowScrollTop);
console.log(windowScrollLeft);

/*
*  Управление прокруткой страницы
*
*  Метод scrollBy(x,y) прокручивает страницу относительно
*  ее текущего положения
*
* */

function setScrollBy() {
    // 0 отвечает за горизонтальную прокрутку
    // 50 отвечает за вертикальную прокрутку
    window.scrollBy(0, 50);
    // получаем в константу количество прокрученных пикселей по вертикали
    const windowScrollTop = window.pageYOffset;
    console.log(windowScrollTop);
}

// прокручивает страницу на абсолютные координаты по оси х и у
// относительно нулевых значений
// чтобы использовать дополнительные параметры метода scrollTo
// его надо записать в другом синтаксисе
function setScrollToOptions() {
    window.scrollTo({
        // тут мы говорим что прокрути нам сверху вниз 500 пикселей а влево 0
        top: 500,
        left: 0,
        // smooth, instant,
        // либо auto; по умолчанию auto
        behavior: "smooth" // тип прокрутки - тут прокрутка будет плавной
    });
}
// опции не работают в safari
// scrollBy добавляет к текущим координатам новые
// scrollTo прокручивает страницу каждый раз когда его вызывают
// от нулевых координат

/*
*  Управление прокруткой страницы
*  Вызов elem.scrollIntoView(top) прокручивает страницу
*  чтобы elem оказался наверху
*  У него есть один аргумент:
*  - если top = true ( по умолчанию ) то страница будет прокручена
*  чтобы elem появился в верхней части окна
*  Верхний край элемента совмещен с верхней частью окна
*  - если top = false то страница будет прокручена чтобы elem
*  появился внизу
*  Нижний край элемента будет совмещен с нижним краем окна
* */

function setScrollIntoView(top) {
    const lessonSelected = document.querySelector('.lesson');
    lessonSelected.scrollIntoView(top);
}

function setScrollIntoViewOptions(top) {
    const lessonSelected = document.querySelector('.lesson');
    lessonSelected.scrollIntoView({
        // "start", "center", "end", "nearest" По умолчанию "center"
        block: "center",
        // "start", "center", "end", "nearest" По умолчанию "center"
        inline: "nearest",
        // "auto" или "smooth" По умолчанию "auto"
        behavior: "smooth"
    }); // опции не работают в safari
}

// Запретить прокрутку
// При это полоса прокрутки может полностью исчезнуть
// и мы при это никак не сможем страницу прокрутить
function setEnableDisableScroll(top) {
    document.body.style.overflow = "hidden";
    document.body.classList.toggle('scroll-lock');
}
/*
*  Для прокрутки страницы ее DOM должен быть полностью построен
* Нарпимер если мы пытаемся прокрутить страницу из скрипта
* <head> это не сработает
*
* */

/*
*  Метрики элементов на странице
*
*  // Получаем объект
*  const block = document.querySelector('.lesson__block');
*  // Позиция объекта
*  // свойства offsetParent, offsetLeft, offsetTop
*
*
*
*
* */
// Получаем объект
const block = document.querySelector('.lesson__block');
//Получаем родительский элемент
// относительно которого позиционирован наш объект
const elementOffsetParent = block.offsetParent;
/*
* Это будет ближайший предок который удовлетворяет следующим условиям:
* 1. Является CSS-позиционированным
* (CSS-свойство position равно absolute, relative, fixed или sticky)
* 2.или теги <td>, <th>, <table>,
* 3. или <body>.
* */
console.log(elementOffsetParent);
// Начнем знакомиться от внешних метрик к внутренним
// получаем позицию объекта относительно предка (offsetParent)
const elemOffsetParent1 = block.offsetParent;
console.log(elemOffsetParent1);
const elemOffsetLeft = block.offsetLeft;
const elemOffsetTop = block.offsetTop;
console.log(elemOffsetLeft);
console.log(elemOffsetTop);

//  Научимся получать обшие размеры объекта - его ширину и высоту
const block2 = document.querySelector('.lesson__block');
// общие размеры объекта
// * при box-sizing: border-box;
const elemOffsetWidth = block2.offsetWidth;
const elemOffsetHeight = block2.offsetHeight;
console.log(elemOffsetWidth);
console.log(elemOffsetHeight);
// отступы внутренней части элмента от внешней
// clientTop и clientLeft
const elemClientTop = block2.clientTop;
const elemClientLeft = block2.clientLeft;
console.log(elemClientTop);
console.log(elemClientLeft);
// Размеры объекта без рамок и полосы прокрутки
// clientWidth и clientHeight
const elemClientWidth = block2.clientWidth;
const elemClientHeight = block2.clientHeight;
console.log(elemClientWidth);
console.log(elemClientHeight);
// общая ширина - offsetWidth
// Рассчитывается так = рамка слева - рамка справа - скролл
// 500 - 20 - 20 - 17 = 443

// Размеры объекта включающие в себя прокрученную (которую не видно) часть
// В остальном работают как clientWidth и  clientHeight
// scrollWodth и scrollHeight
const elemScrollWidth = block2.scrollWidth;
const elemScrollHeight = block2.scrollHeight;
console.log(elemScrollWidth);
console.log(elemScrollHeight);

// Размеры прокрученной области
// scrollLeft и scrollTop
// block.scrollTop = 150;
const elemScrollLeft = block2.scrollLeft;
const elemScrollTop = block2.scrollTop;
console.log(elemScrollLeft);
console.log(elemScrollTop);

// методы управления прокруткой scrollBy, scrollTo, scrollIntoView
// работают и для
// объектов у которых есть полоса прокрутки
function setElementScrollBy() {
    block.scrollBy({
        top: 20,
        left: 0,
        behavior: "smooth"
    });
}

/*
*  Координаты
*  Большинство соответствующих методов JS работает в одной из
*  двух указанных ниже систем координат
*
*  1. относительно окна браузера
*  (как position: fixed, отсчет идет от верхнего левого угла окна.)
*  Принято обозначать clientX/clientY
*  2. Относительно документа
*  (как position: absolute относительно <body>, отсчет идет
*  от верхнего левого угла документа.)
*  Принято обозначать pageX/pageY.
*
*  Когда страница полностью прокручена в самое начало
*  то верхний левый угол окна совпадает с левым верхним
*  углом документа при этом обе этих системы координат тоже совпадают.
*
*  Но если происходит прокрутка, то координаты элементов в контексте
*  окна меняются, так как они двигаются,
*  но в тоже время их координаты относительно документа остаются
*  такими же.
* */
// Получаем объект
const block5 = document.querySelector('.lesson__block');
// Получаем координаты относительно окна браузера
const getItemCoords = block5.getBoundingClientRect();
console.log(getItemCoords);
// top + height = bottom - рассчет bottom объекта относительно окна браузера
// координата нижнего края нашего объекста при отсчете сверху окна
// то же самое будет в метрике rigth
// это положение правого края нашего объекта относительно левого края браузера

// поскольку полученные значения выводятся в виде объекта то мы можем
// получить в константу некую конкретную координату например позицию
// слева - left
// получаем конкретную координату относительно окна браузера
const getItemLeftCoord = block5.getBoundingClientRect().left;
// выводим в консоль и получаем вместо объекта конкретное числовое значение
// нужной нам координаты
console.log(getItemLeftCoord);
// Если мы проскроллим страницу вниз и при этом объект перестанет быть
// видимым на экране то значение top станет отрицательным

//получаем конкретную координаты относительно окна браузера
const getItemTopCoord = block5.getBoundingClientRect().top;
// получаем конкретную координату относительно документа
// pageYOffset - количество прокрученных пикселей по вертикали
const getItemTopDocumentCoord = getItemTopCoord + window.pageYOffset;
console.log(getItemTopCoord);
console.log(getItemTopDocumentCoord);

// мы научились получать координаты объекта - но что если нам нужно
// обратное дествие -
// - Получение объекта по координатам
// - т.е. мы хотим узнать какой объект находится по заданным координатам
// эти координаты относительно окна браузера - от того где находится скролл
const elem55 = document.elementFromPoint(100,100);
console.log(elem55);

/*
*  Домашка
*  1. Изучить теорию
*  2. Решить задачи
*
*  Задача №1
*  Узнать ширину полосы прокрутки
*
*  Задача №2
*  Заставьте браузер прокрутиться на 100px сверху
*  спустя секунду после открытия страницы
*
*  Задача №3
*  Получите координаты любых трех элементов на странице
*
* */











































