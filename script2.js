// тут будет тема с DOM
// Самые верхние элементы дерева доступны как свойства объекта document
const htmlElement = document.documentElement;
const headElement = document.head;
const bodyElement = document.body; // получаем объект body
console.log(htmlElement);
console.log(headElement);
console.log(bodyElement);

//Поулчаем первый и последний дочерние элементы
const firstChildNode = bodyElement.firstChild;
const lastChildNode = bodyElement.lastChild;
console.log(firstChildNode);
console.log(lastChildNode);

//Коллекция childNodes содержит список всех детей включая текстовые узлы
const childNodes = bodyElement.childNodes;
console.log(childNodes);

//Для проверки дочерних узлов существует также специальная функция hasChildNodes()
console.log(bodyElement.hasChildNodes());

// childNodes похож на массив но это не массив а коллекция -
// - особый перебираемый объект - псевдомассив
//Коллекция - отличие от массива в следующем
// 1. Для перебора коллекции мы можем использовать for..of
// 2. Методы массивов не будут работать потому что коллекция это не массив

// Перебор коллекции
for (let node of childNodes) {
    // покажет все узлы из коллекции
    console.log(node);
}

// "Живые" коллекции -
/*  Почти все DOM-коллекции за небольшим исключением - живые
* другими словами они отражают текущее состояние DOM
* Если мы сохраним ссылку на body.childNodes и добавим/удалим
* узлы в DOM то они появятся в сохраненной коллекции автоматически
*
* Только для чтения.
* DOM-коллекции и даже более все навигационные свойства
*перечисленные далее достпны только для чтения
* Мы не можем заменить один дочерний узел на другой просто
*написав childNodes[i] = ...
* Для изменения DOM требуются другие методы
* */

//Навигация по документу
//соседние и родительнские узлы
const previousSiblingNode = bodyElement.previousSibling;
const nextSiblingNode = bodyElement.nextSibling;
const parentNode = bodyElement.parentNode;
console.log(previousSiblingNode);
console.log(nextSiblingNode);
console.log(parentNode);

/*
* Навигационные свойства описанные выше относятся ко всем узлам в документе
* В частности в childNodes находятся и текстовые узлы и
* узлы-элементы и узлы-комментарии если они есть
* */
// Получаем коллекцию всех дочерних узлов - оставим закоментированным
// т.к. объявили выше
// const childNodes = bodyElement.childNodes;
// console.log(childNodes);

/*
* Но для большинства задач текстовые узлы и узлы-комментарии не нужны
* Мы хотим манипулировать узлами-элементами которые представляют собой
* теги и формируют структуру страницы
*
* */

// Получаем коллекцию всех дочерних элементов
const bodyChildren = bodyElement.children;
// тут выводим коллекцию только дочерних html элементов
console.log(bodyChildren);

//Навигация только по элементам
// Первый и последний дочерние ёлементы
const firstChild = bodyElement.firstElementChild;
const lastChild = bodyElement.lastElementChild;
console.log(firstChild);
console.log(lastChild);

// соседние и родительские элементы
const previousSibling = bodyElement.previousElementSibling;
const nextSibling = bodyElement.nextElementSibling;
const parentElement = bodyElement.parentElement;
console.log(previousSibling);
console.log(nextSibling);
console.log(parentElement);

// Поиск произвольного элемента
// elem.querySelectorAll(css)
/*
* Самый универсальный метод поиска
* он возвращает все элементы внутри elem
* удовлетворяющие данному CSS-селектору
* Этот метод действительно мощный
* потому что можно использовать любой CSS-селектор
* */

// Поиск по селектору класса
const elemsOne = document.querySelectorAll('.lesson__list');
console.log(elemsOne);
// Поиск по селектору тега
const elemsTwo = document.querySelectorAll('li');
console.log(elemsTwo);
// Поиск по смешанному селектору тега и класса
const elemsThree = document.querySelectorAll('li.lesson__item-list1');
console.log(elemsThree);
// Поиск по тегу первого уровня вложенности
const elemsFour = document.querySelectorAll('.lesson__list>li');
console.log(elemsFour);
// Поиск по нескольким классам
const elemsFive = document.querySelectorAll('.lesson__list, .lesson__text');
console.log(elemsFive);
// Поиск по вложенным классам
const elemsSix = document.querySelectorAll('.lesson__list .lesson__text');
console.log(elemsSix);
// Поиск по ID
const elemsSeven = document.querySelectorAll('#listItem');
console.log(elemsSeven);
// Поиск по атрибуту
const elemsEight = document.querySelectorAll('[data-item]');
console.log(elemsEight);
// Поиск по атрибуту со значением
const elemsNine = document.querySelectorAll('[data-item="85"]');
console.log(elemsNine);

// Поиск произвольного элемента
// querySelectorAll -  статичная коллекция
// Получение конкретного элемента коллекции
const elems1 = document.querySelectorAll('li');
console.log(elems1[2]);

// for (const item of elems1)
// {
//     console.log(item);
// }

// Можно воспользоваться аналогичным методом forEach
elems1.forEach(item => {
    console.log(item);
});

// Искать можно не только в document
// в константу subList получаем коллекцию объектов которые есть у класса
// .lesson__sub-list
const subList = document.querySelectorAll('.lesson__sub-list');
// берем первый объект subList и внутри него ищем все теги li
const subItems = subList[0].querySelectorAll('li');
console.log(subItems);

// Неожиданный результат поиска
// querySelectorAll - проверяет последний элемент без учета контекста
const sublist2 = document.querySelectorAll('.lesson__sub-list');
const listItems2 = sublist2[0].querySelectorAll('.lesson__list .lesson__item-sub-list');
console.log(listItems2);

// Возвращает первый элемент внутри elem соответствующий данному CSS-селектору
// можно написать как строчкой ниже написано но лучше как второй строчкой
// работать будет быстрее и не нужно перебирать всю коллекцию
//const lessonList2 = document.querySelectorAll('.lesson__list')[0];
// И в константу попадет не коллекция а сразу один объект
const lessonList2 = document.querySelector('.lesson__list');
console.log(lessonList2);

// document.getElementById(ID)
/* Если у элемента есть атрибут id,
* то мы можем получить его вызовом document.getElementById(id),
* где бы он не находился
* */
const elem5 = document.getElementById('listItem');
console.log(elem5);
/*
* 1. id - уникален
* 2. Поиск только внутри document
* */
// в результате мы получаем сразу объект у которого указан данный id


// elem.getElementsByTagName(tag) - Ищет элементы с данным тегом и возвращает
//  (живую коллекцию) их коллекцию. Передав '*' вместо тэга
// можно получить всех потомков.
// При чем поиск может вестись как в объекте document так и в любом
// другом объекте и в итоге
// получаем живую коллекцию всех найденных объектов
const elems6 = document.getElementsByTagName('li');
console.log(elems6);
// тоже самое только поиск по классу - но тут точку не пишем
// потому что и так понятно что мы ищем по имени класса
const elems7 = document.getElementsByClassName('lesson__item-list');
console.log(elems7);
// getElementsByName
// Поиск ведется только внутри объекта document
const elems8 = document.getElementsByName('list');
console.log(elems8);

// Разница между статической и живой коллекцией
// Получаем статическую коллекцию
const listStatic = document.querySelectorAll('.lesson__item-list');
// Получаем живую коллекцию
const listLive = document.getElementsByClassName('lesson__item-list');
console.log(listStatic);
console.log(listLive);
//Создаем новый html-объект
const lessonList3 = document.querySelector('.lesson__list');
lessonList3.insertAdjacentHTML("beforeend",
    '<li class="lesson__item-list">Новый пункт</li>');

//Поиск произвольного элемента
// const elems5 = document.querySelectorAll('.class');

/* closest
* Метод elem.closest(css) ищет ближайшего предка
* который соответствует css - селектору. Сам Элемент
* так же включается в поиск
* Предки элемента это родитель,родитель родителя,
* его родитель и т.д. Вместе они образуют цепочку иерархии
* от элемента до вершины.
*
* Другими словами метод closest поднимается вверх от
* элемента и проверяет каждого из родителей.
* Если он соответствует селектору - поиск прекращается
* Метод возвращает либо предка либо null, если такой элемент
* не найден.
*
* Вспомним что свойство parentElement возвращает только
* непосредственного родителя элемента.
*
* Метод полезный, часто используется для проверки на существование
* того илил иного родителя либо для изменения родителя конкретного
* элемента
*
* */
const elem6 = document.querySelector('.lesson__item-list2');
const parentList3 = elem6.closest('.lesson__list');
console.log(parentList3);

//проверка matches
/* Ничего не ищет а проверет удовлетворяет ли элемент CSS-селектору
* и возвращает true или false
*
* */
const elems88 = document.querySelectorAll('.lesson__list');
for (let elem of elems88) {
    if (elem.matches('[class$="lesson__list_red"]')) {
        console.log('Красный');
    } else if ( elem.matches('[class$="lesson__list_blue"]')) {
        console.log('Синий');
    }
}

/* Стоит добавить что получив объект тем или иным способом мы можем
применять свойства навигации о которых говорили ранее
* */
const text55 = document.querySelector('.lesson__text');
const list55 = text55.nextElementSibling;
console.log(list55);

// Содержимое элемента innerHTML
//получаем объект
const textElement55 = document.querySelector('.lesson__text');
//получаем содержимое объекта как есть вместе с HTML
const textElementContent = textElement55.innerHTML;
console.log(textElementContent);
//Дописываем содержимое объекта
textElement55.innerHTML = `<p>${textElementContent}</p>
                           <p>Живи а работай в свободное время!</p>`;
console.log(textElement55.innerHTML);

//Содержимое элемента целиком outerHTML
// Получаем объект
const textElement77 = document.querySelector('.lesson__text');
//Получаем содержимое объекта как есть
//вместе с HTML а так же сам элемент
// const textElementContent77 = textElement77.outerHTML;
// console.log(textElementContent77);
// textElement77.outerHTML = `<p>Живи а работай в <span class="yellow">свободное</span> время</p>`;
// console.log(textElement77.outerHTML);

// НЕ ПАШЕТ - РАЗОБРАТЬ ПОЧЕМУ !!!
// просто текст элемента textContent
// получаем объект
const textElement8885 = document.querySelector('.lesson__text');
const textElementContent8885 = textElement8885.textContent;
console.log(textElementContent8885);
/* Полезная возможность textContent - записывать
* текст безопасным способом
*
* Мы не хотим чтобы на сайтп появлялся произвольный HTML-код.
* Присваивание через textContent - один из способов от этого
* защититься.
* */

// Если мы хотим поработать  не с элементом HTML-тегом а с
/** неким другим узлом текстовым или например комментарием
 * в HTML-коде документа
 * для этого мы можем использовать data которая вернет нам содержимое
 *текстового узла либо комментария
 *
 */
// получаем объект
const textElement8888 = document.querySelector('.lesson__text');
const getComment1 = textElement8888.nextSibling;
console.log(getComment1);
// чтобы получить сам текст комментария используем свойство data
console.log(getComment1.data);
// мы можем изменить наш комментарий либо другой текстовый узел
getComment1.data = 'hi';
console.log(getComment1.data);

// Создание элементов и узлов
// создание нового элемента (тега)
const newElement33 = document.createElement('div');
console.log(newElement33);
// наполняем наш div контентом
newElement33.innerHTML = `Живи а работай в сободное время`;
console.log(newElement33);

// Создание элементов и узлов
//Создание нового элемента (тега)
const newElement = document.createElement('div');
//Наполняем новый элемент
newElement.innerHTML = `Живи а работай в свободное время`;
console.log(newElement);
//Создание нового текстового узла
const newText = document.createTextNode('Привет');
console.log(newText);
/* Созданные объекты находятся в константах
*  но не являются частью документа
* */

//Методы вставки
// Получаем объект
const textElement2 = document.querySelector('.lesson__text');
// Создание нового элемента тега
const newElement2 = document.createElement('div');
//Наполняем новый элемент
newElement.innerHTML = `Живи а работай в свободное время`;
console.log(newElement);
// Вставояем новый объект
//...перед объектом
textElement2.before(newElement2);
//...после объекта
// textElement2.after(newElement2);
//...внутрь и в начало объекта
// textElement2.prepend(newElement2);
//...внутрь и в конец объекта
// textElement2.append(newElement2);

// Вставка нескольких фрагметов сразу
textElement2.append(newElement2, "Привет!");

//Методы вставки
// insertAdjacentHTML/Text/Element
// Получаем объект
const textElement222 = document.querySelector('.lesson__text');
// Вставляем текст, HTML, элемент
textElement222.insertAdjacentHTML(
  'afterend',
         `<p>Живи, а работай в 
                <span class="blue">свободное</span> время!</p>`
);
/*
*   beforebegin - вставить html непосредственно перед textElement222
*   afterbegin - вставить html в начало textElement222
*   beforeend - вставить html в конец textElement222
*   aftereend - вставить html непосредственно после textElement222
* */

/* Дополнительно существуют
*  insertAdjacentText и insertAdkacentElement
* */
// Получаем объект
const textElement2222 = document.querySelector('.lesson__text');
// Вставляем текст, HTML, элемент
textElement2222.insertAdjacentText(
    'afterend',
    `<p>Живи, а работай в 
                <span class="blue">свободное</span> время!</p>`
);
// Создание нового элемента тега
const newElement3333 = document.createElement('div');
//Наполняем новый элемент
newElement3333.innerHTML = `Живи а работай в свободное время`;
// Вставляем элемент
textElement2222.insertAdjacentElement(
  'beforeend',
          newElement3333
);
// На практике часто используют insertAjacentElement
/* потому что для элементов и текста у нас есть методы
*  append/prepend/before/after - их быстрее написать
*  и они могут вставлять как узлы так и текст
* */

/* Закрепим знания
*  Методы вставки insertAdjacentHTML /Text/Element
*  lesson__list это наш объект
*  <ul class="lesson__list">
   <!-- afterbegin -->
*  <li>Пунт №1</li>
*  <li>Пунт №2</li>
*  <li>Пунт №3</li>
*  <!-- beforeend -->
   </ul>
*  <!-- afterend -->
*
* */

// перенос элемента
/* мы можем вставлять не только новые узлы
 но и переносить существующие
 Все методы вставки автоматически
 удаляют узлы со старых мест
*
* */
// Получаем объект
const lessonBlock = document.querySelector('.lesson');
// Получаем объект
const title = document.querySelector('h3');
// Переносим title в конец блока lessonBlock
lessonBlock.append(title);

// клонирование узлов cloneNode
// Если нужен не перенос а именно копия элемента
// Получаем объект
const lessonBlock1212 = document.querySelector('.lesson');
// Получаем объект
const textElement2323 = document.querySelector('.lesson__text');
// Клонирование без дочерних элементов
const cloneTextElement = textElement2323.cloneNode();
lessonBlock1212.append(cloneTextElement);

// глубокое клонирование вместе с содержимым
// Получаем объект
const lessonBlock121212 = document.querySelector('.lesson');
// Получаем объект
const textElement232323 = document.querySelector('.lesson__text');
const cloneTextElement1 = textElement232323.cloneNode(true);
lessonBlock121212.append(cloneTextElement1);

// Удаление узлов
// Получаем объект
const lessonBlock1212123 = document.querySelector('.lesson');
// Получаем объект
const textElement2323233 = document.querySelector('.lesson__text');
const cloneTextElement13 = textElement2323233.cloneNode(true);
lessonBlock1212123.append(cloneTextElement13);
// Удаляем объект
textElement2323233.remove();

/* Стили и классы
*
*  Управление классами
*
*  Свойства className и classList
*
*  Изменение класса является одним
*  из наиболее часто используемых действих в JavaScript
*
*  Свойство className
* */
//получаем элемент
const element8888 = document.querySelector('.lesson__list_blue');
// Получаем имена классов
const elementClassName = element8888.className;
console.log(elementClassName);
// перезаписываем имя класса - но таким способом мы перезапишем все классы
element8888.className = "red";

/* свойство classList
*  Специальный объект с методами для добавления/удаления одного класса
*
*  element7777.classList.add('active'); active это класса который
*  мы добавляем для элемента html
*  в CSS мы можем как угодно изменить стили для данного объекта
*  с классом active
*
* */
//получаем элемент
const element7777 = document.querySelector('.lesson');
// добавить класс
element7777.classList.add('active');
// удалить класс
// element7777.classList.remove('active');
// // добавить класс если его нет а если есть удалить
// element7777.classList.toggle('active');
// // проверка наличия класса - возвращает true/false
// element7777.classList.contains('active');

//получаем элемент
const element5757 = document.querySelector('.lesson__text');
// добавить класс
element5757.classList.add('active1');
// проверяем наличие класса
if (element5757.classList.contains('active1')) {
    console.log(` У element5757 есть класс active`);
}
// classList является перебираемым поэтому можно перечислить все
// классы при помощи for..of
for (let className of element5757.classList) {
    console.log(className);
}

//Управление стилями
// element.style
//получаем элемент
const element57575 = document.querySelector('.lesson__item-list2');
// задаем стиль с помощью CSS свойства
element57575.style.color = "red";
// для свойства из несокольких слов используется camelCase:
// margin-bottom
element57575.style.marginBottom = "30px";
// z-index
element57575.style.zIndex = "10";
// получение значения свойства - для этого достаточно к нему обратиться
// только если оно записано в атрибуте style
console.log(element57575.style.marginBottom);
// сброс стиля
element57575.style.marginBottom = "";

// каждое css свойство нужно писать отдельно и это не очень удобно
// когда нам нужно записать их множество
// полная перезапись стилей
// style.cssText - минус в том что cssText полностью перезаписывает
// все css свойства которые были до этого в атрибуте style
// получаем элемент
const elem1234 = document.querySelector('.lesson__item-list3');
elem1234.style.cssText = `
    margin-bottom: 30px;
    color: green;
`;

/* Вычисленные стили
   getComputedStyle(element, [pseudo])
*
*  получение стилей с помощью  getComputedStyle(element, [pseudo])
*  иными словами если у нас в html коде не записаны для конкретного элемента
*  стили в style то  getComputedStyle(element, [pseudo]) делает возможным
   добавление стилей без атрибута style в html коде
* */
// получаем элемент
const elem12345 = document.querySelector('.lesson__item-list3');
// получение значения свойства только если оно записано в атрибуте style
// без использования getComputedStyle наш console.log выведет пустую
// строку в консоль
console.log(elem12345.style.fontSize);
// Стиль элемента
const elemStyle12345 = getComputedStyle(elem12345);
console.log(elemStyle12345.fontSize);
// Стиль псевдоэлемента
const elemBeforeStyle = getComputedStyle(elem12345, "::before");
console.log(elemBeforeStyle.backgroundColor);

// Чтобы получить точное значение какого нибудь конкретного свойства
// то следует писать его полное название
// иными словами чтобы получить конкретное значение
// следует писать точное (полное ) свойство
// получаем точное значение
console.log(elemStyle12345.paddingLeft);
// Получаем непредсказуемую записать
console.log(elemStyle12345.padding);

// Работа со значениями CSS свойств с помощью getComputedStyle
// работает только для чтения - т.е. таким образом мы не можем
// переназначить то или иное свойство

// В целом мы используем getComputedStyle для того чтобы получить
// текущее значение того или иного свойства
// например получить текующуюю ширину для того чтобы добавить что то
// отнять что то, изменить - одним словом нам нужно число
// поэтому когда мы используем
// console.log(elemStyle12345.paddingLeft);
// console.log(elemStyle12345.padding);
//  то мы получаем строку еще и с пикселями которые нам не нужны
// так вот удобно получать такие стили с помощью parseInt
// которые оставят нам только цифры
// получаем элемент
const elem123456 = document.querySelector('.lesson__sub-list');
// Стиль элемента
const elemStyle123456 = getComputedStyle(elem123456);
console.log(elemStyle123456.paddingLeft);
// получаем число
const paddingLeft = parseInt(elemStyle123456.paddingLeft);
console.log(paddingLeft);
// помним про единицы измерения
elem123456.style.marginLeft = "20px";

/*
*  Стили и классы
*
*  Как мы уже поняли из JS мы можем управлять как классами
*  так и стилями объекта
*
*  Управлять классами - более приоритетный вариант по сравнению
*  со стилями
*
*  Манипулировать свойством style следует только в том случае
*  если классы нам помочь не могут
*  Например при изменении координатов объектов на лету
*
*  Одним словом если мы не можем возложить решение
*  той или иной задачи на плечи CSS путем манипуляции с классами
*  то так и следует сделать
*
* */

/*
*  Атрибуты и свойства
*
*  У разных DOM элементов могут быть разные свойства
*  Например у тега <a> есть свойства связанные со ссылками
*  а у тега <input> свойства связанные с полем ввода и т.д.
*
*  В HTML у тегов могут быть атрибуты
*  когда браузер парсит HTML чтобы создать
*  DOM-объекты для тегов он распознает стандартные
*  атрибуты и создает DOM-свойства для них
*
*  Каждый DOM-узел принадлежит соответствующему встроенному классу
*
* */
const link1 = document.querySelector('.lesson__link');
const input1 = document.querySelector('.lesson__input');
console.log(link1.href);
console.log(input1.href);
console.log(link1.value);
console.log(input1.value);
// получить список свойств для того или иного элемента
console.dir(link1);

/*
*  Произвольные атрибуты
*
*  Мы можем установить произвольные атрибуты для элементов
* */
const link2 = document.querySelector('.lesson__link');
// Проверяем значение атрибута
link1.hasAttribute('name');
// получаем значение атрибута
link1.getAttribute('name');
// Устанавливаем значение атрибута
// link1.setAttribute('name', 'value');
// Удаляем атрибут
link1.removeAttribute('name');

// Разберем практический пример
const link3 = document.querySelector('.lesson__link');
link2.setAttribute('some-attribute', 'some-value');
// проверяем наличие атрибута
if (link2.hasAttribute('some-attribute')) {
    console.log('some-attribute существует!');
}

/*
*  Синхронизация между атрибутами и свойствами
*
*  Мы можем обратиться к тому или иному свойству через методы
*  доступа к атрибутам
*  когда стандартный атрибут изменяется - соответствующее
*  свойство автоматически обновляется
*
*  Это работает и в обратную сторону (за некоторыми исключениями)
* */
const input2 = document.querySelector('.lesson__input');
// установим значение атрибута id равным 123
input2.setAttribute('id', '123');
// получим его свойство и выведем в консоль это значение
console.log(input2.id);
// перезапишем его свойство новым значением
input2.id = "321";
// получим перезаписанное свойство и выведем его в консоль
console.log(input2.getAttribute('id'));

// со свойством value это не работает
const input3 = document.querySelector('.lesson__input');
input3.setAttribute('value', 'Привет');
console.log(input3.value);
input3.id = "Как дела?";
console.log(input3.getAttribute('value'));

/*
*  Нестандартные атрибуты - dataset
*
*  Мы уже использовали произвольные атрибуты но это рискованно
*
*  Все атрибуты начинающиеся с префикса data-
*  зарезервированны для использования программистами
*  Они доступны в свойстве dataset
*
*
* */
// Получаем элемент
const input5 = document.querySelector('.lesson__input');
// Получаем data-атрибут
console.log(input5.dataset.size);
// Перезаписываем data-атрибут
input5.dataset.size = "54321";
console.log(input5.dataset.size);
// Если имя data-атрибута состояло бы из двух и больше слов то в JS
// стоило бы указывать имя в стиле lower camel case
// например data-size-value
console.log(input5.dataset.sizeValue);

/*
*  полезные свойства
*
*  Если мы вдруг забыли какой тег у нашего объекта
*  мы можем себе это напомнить с помощью свойства .tagName
*  в результате получим имя тега в верхнем регистре
*
*  Часто требуется объект скрыть или показать
*  Для этого мы можем использовать свойство .hidden
* */
// Получаем элемент
const input6 = document.querySelector('.lesson__input');
// Получаем тег элемента
console.log(input6.tagName);
// Скрыть/Показать элемент - если true то в данном случае input исчезнет
// input6.hidden = true;
console.log(input6.hidden);

// Домашка
/* Задача №1

получить в переменную элемент с атрибутом data-say-hi
*  и прочитать значение этого атрибута
*
*  <div data-say-hi="yes">Привет!</div>
* */



/* Задача №2
*
*  Получить в переменную элемент с текстом Йончи
*
*  <ul>
    <li>Йончи</li>
    <li>Йончи</li>
   </ul>
*
* */


/* Задача №3
*
*  Получить в переменную коллекцию элементов с классом like
*
*   <div class="like"></div>
    <div class="subscribe"></div>
    <div class="like subscribe"></div>
*
* */


/* Задача №4
*
*  Куда добавится <li>Текст</li> ?
*
*  //JS
*  const list = document.querySelectorAll('ul');
*  list.insertAdjacentHTML(
*  'beforeend'
*  '<li> Текст </li>'
*  );
*
*  // HTML
*  <ul>
    <li>Пункт1</li>
    <li>Пункт2</li>
   </ul>
*
* */








