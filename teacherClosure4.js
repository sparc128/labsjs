/*
* Лаб работа #4
*
* Closure
*
* Замыкание это возможность функции которая находится внутри другой функции
* получать доступ к переменным которые находятся в области видимости
* родительской функции даже после того как родительская функция
* прекратила свое выполнение
*
* Пример 1:
* */

// Тут пока нет замыкания
function external() {
    const externalVar = 'Я внешнаяя функция';

    function internal(){
        const internalVar = 'Я внутренняя функция';
        console.log('internalVar > ', internalVar);
        console.log('externalVar >', externalVar);
    }
    internal();
}

external();

/*
* Чтобы создать замыкание
* 1. вставить return двумя способами:
* 1.1. return function internal(){
* 1.2. return internal;
* 2. создадим переменную и присвоим ей функцию external();
* const internalFN = external();
* 3. проверяем в консоли вывод функции internal
* console.log(internalFN);
*
* Если запустить внутреннюю функцию internalFN() за пределами
* функции external - то будет ли доступна переменная externalVar
* внутри функции external() ведь после ее запуска она прекратит
* свое действие ?
*
*Что получается - мы можем присвоить external()
* какой-нибудь переменной у нас появляется доступ
* к внутренней функции internal()
* и через какое то время мы можем использовать эту
* внутреннюю функцию за пределами нашей функции external()
* запуская ее в нашей строчке internalFN();
*
* Идея замыкания заключается в том что несмотря
* на то что external() заканчивает свое выполнение
* JS оставляет в памяти переменную externalVar
* и она остается доступной внутри нашей функции
* internal() - она (переменная) не уничтожается
* и мы можем использовать ее за пределами функции
* external
*
* Пример 2:
* */
function external() {
    const externalVar = 'Я внешнаяя функция';

    function internal(){
        const internalVar = 'Я внутренняя функция';
        console.log('internalVar > ', internalVar);
        console.log('externalVar >', externalVar);
    }
    return internal;
}
const internalFN = external();
console.log(internalFN);
internalFN()

/*
*
* Механика -
* когда мы создаем внешнюю функцию createAddress() - то мы создаем
* переменную address внутри внешней функции createAddress()
* и далее эта переменная становится доступна внутри
* нашей безымянной функции
* return function (name) {
        return `${address} ${name}`;
   }
   *
   * т.е. наша внутренняя безымянная функция имеет доступ к переменной
   * из области видимости родительской функции даже после того как
   * наша родительская функция завершила свое выполнение
   * т.е. const addressGrazhdanin = createAddress('Гражданин');
   * функция createAddress отработала и мы все равно получаем доступ к
   * console.log(addressGrazhdanin('Василий'))
   * переменной address внутри безымянной функции уже после того
   * как внешняя функция createAddress отработала
   *
   * т.е. родительская функция перешла в статус closed
   * отработав вот тут const addressGrazhdanin = createAddress('Гражданин');
   * - завершив свое действие и несмотря на это мы имеем доступ к
   * нашему пространству где находится наша переменная address
   * и это называется clojure - замыканием
   *
   * Пример 3:
* */
function createAddress(type) {
    const address = type.toUpperCase();
    return function (name) {
        // возьмем переменную address из области видимости родительской функции
        // и переменную name которую будет передавать внутрь этой функции
        return `${address} ${name}`;
    }
}

const addressGrazhdanin = createAddress('Гражданин');
const addressGrazhdanka = createAddress('Гражданка');
// проверяем вывод безымянной функции  return function (name)
console.log(addressGrazhdanin);
console.log(addressGrazhdanin('Василий'))
console.log(addressGrazhdanka('Александра'))

/*
* name - имя игрока
* score - подсчет очков
*
* Даже несмотря на то что мы используем одну переменную score,
* благодаря тому что мы создали при вызове две внутренние функции
* scoreCount() - каждой из этих внутренних функций доступна
* перенная score со своими значениями
* const playerOne = createPlayer('Василий')
  const playerTwo = createPlayer('Анна')
*
* Еще раз - замыкание - это возможность для внутренней функции
* получать доступ к переменным которые находятся в области видимости
* родительской функции - в нашем случае переменная score -
* даже после того как родительская функция завершила свое выполнение
*
* Пример 4:
* */
function createPlayer(name) {
    let score = 0;
    return function scoreCount() {
        score++;
        // тут score текущее значение баллов игрока
        return `${name} - ${score} баллов!`;
    }
}

// в консоли запускаем playerOne()
// например может быть полезно чтобы вести счет для нескольких игроков
// одновременно
const playerOne = createPlayer('Василий')
const playerTwo = createPlayer('Анна')









