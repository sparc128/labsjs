// объекты в js

// создание объекта

//let userInfo = new Object(); // синтаксис "конструктор объекта"
//let userInfo = {}; // синтаксис "литерал объекта"
// обе эти строчки создадут пустой объект

// свойства объектов ключ + значение
let userInfo2 = {
    age: 30, // свойство объекта
    name: "Вася", // последняя висячая запятая
    "like js": true,
};

// получаем весь объект
console.log(userInfo2);
// получаем свойство объекта
console.log(userInfo2.name); // аналог console.log(userInfo2['name']); тип кавычек значения не имеет
console.log(userInfo2["like js"]);

// Имена свойств
// Вычисляемое либо передаваемое имя
// Вычисляем имя
let firstPart = "likes";
let userInfo3 = {
    name: "Саша",
    age: 30,
    [firstPart + "javascript"]: true,
};
console.log(userInfo3["likes javascript"]);

// Просто передаем имя свойства
let firstPart1 = "likes";
let userInfo4 = {
    name: "Саша",
    age: 30,
    [firstPart1]: true,
};
console.log(userInfo4[firstPart1]);

//Преимущество квадратных скобок
let key = "name";
console.log(userInfo4[key]);

// Зарезервированные слова в именах свойств объектов - там мы можем их использовать
let userInfo5 = {
    let: "Вася",
    for: 30,
};
console.log(userInfo5.let);
console.log(userInfo5.for);

//Имена свойств
//Имена = строки либо символы
let userInfo6 = {
    0: "Вася", // 0 тоже самое что "0"
};
console.log(userInfo6[0]);
console.log(userInfo6["0"]);

//Тип данных Symbol
//Создаем символ id с описанием (именем) "id"
let id = Symbol("id");
let userInfo7 = {
    name: "Вася",
    age: 30,
    [id]: "Некое значение"
};
console.log(userInfo7);
// Основное применение символов:
// 1. Скрытие свойство объекта
// Символьное свойтсво не появится в for..in
// 2. Использование системных символов
// Symbol.iterator, Symbol.toPrimitive и т.д.

//Вложенность
let userInfo8 = {
    name: "Вася",
    age: 30,
    addres: {
        city: "SPB",
        street: "Freedom",
    }
}
console.log(userInfo8);
console.log(userInfo8.addres);
console.log(userInfo8.addres.city);
console.log(userInfo8.addres.street);

// Свойство из переменной
function makeUserInfo(name, age) {
    return {
        name: name,
        age: age,
        //..др свойства
    };
}
let user1 = makeUserInfo("Вася", 30);
console.log(user1);

// Свойство из переменной 2
function makeUserInfo2(name2, age2) {
    return {
        // когда имя переменной равно имени получаемого значения то мы можем записать просто одно имя - эквивалентно записи примера выше
        name2, // тоже самое что и name2: name2
        age2,   // тоже самое что и age2: age2
        "likes js": true,
        //..др свойства
    };
}
let user2 = makeUserInfo2("Вася", 30);
console.log(user2);

//Изменение объекта
//Добавление свойства
let userInfo9 = {
    name: "Вася",
}
console.log(userInfo9);
userInfo9.age  = 30; // добавление свойства
console.log(userInfo9);
userInfo9['likes js'] = true;
console.log(userInfo9);
userInfo9.addres = {
    city: "SPB",
    street: "FREEDOM",
};
console.log(userInfo9);

// Удаление свойства
let userInfo10 = {
    name: "Вася",
    age: 30,
    "like js": true,
}
console.log(userInfo10);
delete userInfo10.age;
console.log(userInfo10);
delete userInfo10["like js"];
console.log(userInfo10);

//Измение свойства
let userInfo11 = {
    name: "Вася",
    age: 30,
    "like js": true,
}
console.log(userInfo11);
userInfo11.age = 18;
console.log(userInfo11);

//Изменение свойства даже в константе
const userInfo12 = {
    name: "Вася",
    age: 30,
    "like js": true,
}
console.log(userInfo12);
userInfo12.age = 15;
console.log(userInfo12);

//Копирование объектов
//При копировании объекта в другую переменную сам объект не дублируется
//а копируется только ссылка на него
let userInfo15 = {
    name: "Вася",
    age: 30,
    "like js": true,
}
console.log(userInfo15);
let user5 = userInfo15;
console.log(user5);
user5.age = 12;
console.log(user5);

//Дублирование объектов (Оbject.assign)
// Синтаксис
//Object.assign(куда(объект),что(свойство #1), что(свойство #2), ...);
let userInfo16 = {
    name: "Вася",
    age: 30,
    "like js": true,
}
let user8 = Object.assign({}, userInfo16)
user8.age = 35;
console.log(userInfo16);
console.log(user8);

//Добавление нового значения при помощи Object.assign
let userInfo17 = {
    name: "Вася",
    age: 30,
}
Object.assign(userInfo17, {["like js"]: true, city: "SPB"});
console.log(userInfo17);

//Проверка существования свойства объекта 1
let userInfo18 = {
    name: "Вася",
    age: 30,
}
console.log(userInfo18.age);
if (userInfo18.age) { // true или false
    console.log(userInfo18.age);
}

//Проверка существования свойства объекта 2 - Синтаксис Опциональная цепочка
let userInfo20 = {
    name: "Вася",
    age: 30,
    address: {
        city: "SPB",
        street: "Freedom",
    }
}
console.log(userInfo20?.address?.city);
// или так
if (userInfo20?.address?.city) {
    console.log(userInfo20.address.city);
}

//Оператор in
let userInfo21 = {
    name: "Вася",
    age: 30,
    address: {
        city: "SPB",
        street: "Freedom",
    }
}
if ("name" in userInfo21) {
    console.log(userInfo21.name);
}

/* В большинстве случаев сработает сравнение с undefined.
*  либо опциональная цепочка ?.
*  Но есть особый случай, когда свойство существует,
*  но содержит значение undefined.
*  В это случае необходимо использовать "in".
*
* */
let userInfo22 = {
    name: undefined,
    //...следующие свойства
}
// этот код не сработает
// if (userInfo22.name) { // false
//     console.log(userInfo22.name);
// }
// а этот сработает
if ("name" in userInfo22) { // true
    console.log(userInfo22.name);
}

// Цикл for..in для перебора всех свойств объекта
//Этот цикл отличается от изученного ранее цикла for(;;)
//for (let key in object) {
// тело цикла выполняется для каждого свойства объекта
// key - ключи - это имена свойств
//}

let userInfo23 = {
    name: "Вася",
    age: 30,
    address: {
        city: "SPB",
        street: "Freedom",
    }
}

for (let key in userInfo23) {
    // ключи
    console.log(key); // name, age, address
    // значения ключей
    console.log(userInfo23[key]); // Вася, 30, Object{}
}

for (let key in userInfo23.address) {
    // ключи
    console.log(key); // city, street
    // значения ключей
    console.log(userInfo23.address[key]); // SPB, Freedom
}

//Методы объекта - в качетсве свойства объекта может быть функция
let userInfo24 = {
    name: "Вася",
    age: 30,
    address: {
        city: "SPB",
        street: "Freedom",
    },
    // showInfo22: function () {
    //     console.log(`${userInfo24.name}, ${userInfo24.age} лет. Адрес: г. ${userInfo24.address.city}, ул.${userInfo24.address.street}`);
    // },
    // более короткая запись
    showInfo22 () {
        console.log(`${userInfo24.name}, ${userInfo24.age} лет. Адрес: г. ${userInfo24.address.city}, ул.${userInfo24.address.street}`);
    },
}
userInfo24.showInfo22();

// использование this в методах объекта
// словом this можно заменить текущий объект
let userInfo25 = {
    name: "Вася",
    age: 30,
    address: {
        city: "SPB",
        street: "Freedom",
    },
    showInfo23 () {
        console.log(`${this.name}, ${this.age} лет. Адрес: г. ${this.address.city}, ул.${this.address.street}`);
    },
}
userInfo25.showInfo23();

// использование this в методах объекта 2
let userInfo26 = {
    name: "Вася",
    age: 30,
    address: {
        city: "SPB",
        street: "Freedom",
    },
    showInfo24 () {
        // если мы внутри этой функции напишем еще одну НЕстрелочную функцию со словом function то работать не будет - будет работать как в примере на след строчке
        // работает т.к. у стрелочной функции нет своего this
        let show = () => console.log(`${this.name}, ${this.age} лет. Адрес: г. ${this.address.city}, ул.${this.address.street}`);
        show();
    },
}
userInfo26.showInfo24();

// Преимущество this
// использование this надежнее чем использование имени переменной которой непосредственно присвоен объект
let userInfo27 = {
    name: "Вася",
    age: 30,
    address: {
        city: "SPB",
        street: "Freedom",
    },
    showInfo25 () {
        console.log(`${this.name}, ${this.age} лет. Адрес: г. ${this.address.city}, ул.${this.address.street}`);
    },
}
userInfo26.showInfo24();
// будет работать потому что у нас нет привязки к конкретной переменной
// у нас есть привязка к конкретному объекту
let user = userInfo27;
userInfo27 = null;
user.showInfo25();

// Функция-конструктор
/*
* Обычный синтаксис создания объекта {...} позволяет создать
* только один объект. Но зачастую нам нужно создать множество
* однотипных объектов, таких как пользователи, элементы меню и т.д.
* Это можно сделать при помощи функции-конструктора и оператора "new"
*
* Функции-конструкторы являются обычными функциями.
* Но есть два правила:
* 1. Имя функции-конструктора должно начинаться с большой буквы
* 2. Функция-конструктор должна вызываться при помощи оператора new
*
* */
function UserInfo30(name) {
    // this = {}; создается пустой объект (неявно)
    this.name = name;
    this.age = 30;
    // return this; возвращается объект (неявно)
}
console.log(new UserInfo30('Вася'));
console.log(new UserInfo30('Саша'));







