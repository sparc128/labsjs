
const div = document.createElement('div');
div.classList.add('wrapper1');
const body = document.body;
body.appendChild(div);
const header = document.createElement('h1');
header.textContent = 'DOM (Document Object Model)';
div.insertAdjacentElement('beforebegin', header)

const ul = `
    <ul>
        <li>Один</li>
        <li>Два</li>
        <li>Три</li>
    </ul></ul    
`;
div.innerHTML = ul;

const img1 = document.createElement('img');
img1.src = 'https://picsum.photos/240';
img1.width = 240;
img1.classList.add('super');
img1.alt = 'Super man';
div.appendChild(img1);

const elemHTML = `<div class="pDIV">
    <p>Параграф 1</p>
    <p>Параграф 2</p>
</div>`;

const ulList = div.querySelector('ul');
ulList.insertAdjacentHTML('beforebegin', elemHTML);
const pDiv = document.querySelector('.pDIV');
pDiv.children[1].classList.add('text');
pDiv.firstElementChild.remove();

const generateAutoCard = (brand, color, year) => {
    const curDate = new Date();
    const curYear = curDate.getFullYear();
    return `
          <div class="autoCard">
             <h2>${brand.toUpperCase()} ${year}</h2>
             <p>Автомобиль ${brand.toUpperCase()} - ${year} года. Возвраста авто - ${curYear - year} лет.</p>
             <p>Цвет: ${color} </p>
             <button class='btn3' type='button'>Удалить</button>
          </div>
    `;
}


const carsDiv = document.createElement('div');
carsDiv.classList.add('autos');

const carsList = [
    {brand: 'Tesla', year: 2015, color: 'red'},
    {brand: 'Lexus', year: 2016, color: 'silver'},
    {brand: 'Nissan', year: 2012, color: 'black'},
];

const carsHTML1 = carsList.map(car => {
    return generateAutoCard(car.brand, car.color, car.year);
}).join('');
carsDiv.innerHTML = carsHTML1;
div.insertAdjacentElement('beforebegin', carsDiv);

const buttons3 = document.querySelectorAll('.btn3');
function handleClick(e) {
    const currentButton = e.currentTarget;
    currentButton.closest('.autoCard').remove();
};

buttons3.forEach(button11 => {
    button11.addEventListener('click', handleClick);
});






































