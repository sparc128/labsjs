/*
* ES6
*
* Лабораторная работа #2.
* Название: Event JS.
* Цели:
* 1. Научиться Использовать Event JS
* 2. Понять фазу Всплытие
* 3. Понять фазу Погружение
* 3. Дать базовое понимание события языка JavaScript
* 4. Отработать практические примеры событийной модели JS
* 5. Сформулировать выводы, записать exel файлом, прислать в лк и почту.
* Ход работы:
* 1. Открыть codepen.io
* 2. Ввести листинг кода представленный ниже.
* 3. Получить результаты
* 4. Сформулировать, записать выводы
* 5. Прислать на проверку в лк, и почту.
*    * Данные вопросы задают на собеседованиях.
* Отчет:
* 1. Написать цели работы
* 2. Написать что студент делал в ходе выполнения работы своими словами
* 3. Написать вывод о проделанной работе
* 4. *Если работа содержит листинг кода - написать листинг кода
* 5. *Если работа содержит рисунки - добавить рисунки в отчёт
* 6. *Если работа содержит вывод в Console браузера, Elements, Sources,
*     Вывод информации в окно браузера иными словами любые результаты
*     работы стандарта html5** то их надо вносить в отчет в свободной форме
*     **html5 - стандарт вклюает в себя html4 и ниже, CSS3 и ниже, ES6 и ниже
* Event
* */

/*
* Подключаемся к btn
* const buttons = window.document.querySelector('.btn');
* Выводим в консоль в NodeList все кнопки
* console.log(buttons);
* */
const buttons = window.document.querySelectorAll('.btn');
// console.log(buttons);

/*
* 1.Используем цикл чтобы прочитать все элементы нашего списка
* из NodeList в данном случае
* buttons.forEach(button => {
    button.addEventListener('click', handleClick)
  })
*
* 2.Вешаем обработчик событий на каждую конкретную кнопку
* button.addEventListener('click', handleClick)
*
* 3.Выводим в консоль результаты работы
* стрелочной функции handleClick
*
* */
//const handleClick = () => { /* console.log('Click') */ }
//buttons.forEach(button => { button.addEventListener('click', handleClick) })

/*
* Как нам понять на какой именно кнопке щелкнул пользователь?
* Для этого используем объект event
* Который доступен в нашей call-back функции
* Который мы передаем в наш обработчик addEventListener
*
* Добавим атрибут data-number=" " в во все теги button
* в файл teacher2All.html
*  * data - обязательно так и пишем data
*    number - это слово мы задаем произвольно
*    слова в этом атрибуте пишем через дефиз data-number
* и нумеруем значения атрибута data-number="1"
* значениями по порядку
*
* Свойство .dataset дает доступ к нашему атрибуту data
* для каждого элемента button
*
* Свойство .number дает доступ к строчному значению атрибута data-number
*
* Метод parseInt() - получаем цифровое значение атрибута data-number
*
* event.target - целевой элемент
* Целевой элемент - элемент на котором произошло событие
* Целевой элемент - элемент на котором мы кликнули мышкой
* event.target - Используется для получения информации об этом элементе
*
* Еще пример - получим текстовое содержимое .textContent
*
* Разница между event.target и event.currentTarget
* Пометим наши обработчики событий:
* "target >" это event.target
* "currentTarget >" это event.currentTarget
* Для того, чтобы понять разницу между event.target и event.currentTarget
* добавим вложенность в html код
*
* Сравним event.target и event.currentTarget
* console.log(event.target === event.currentTarget)  // false
*
* При клике на кнопке click1
* 1. target > <strong>1</strong> - это целевой элемент
*    на который мы изначально кликнули
* 2. currentTarget > <button data-number="1" class="btn">Click1</button>
*    это элемент на который мы повесили обработчик событий
*    где это событие сработало
*    Как запомнить - currentTarget это наш button
*    который нахоидится слева от addEventListener
* 3. и результат false
*
* Добавим обработчик для всего окна браузера
* window.addEventListener('click', function () { console.log('Window click!')})
*
* ФАЗА ВСПЛЫТИЯ - механика работы
* В браузере кликнем в любой его области - в консоль выведется Window click!
* Итак кликнем по кнопке Click1 - мы получим вывод который был ранее для
* этой кнопки и так же надпись Window Click!
* Таким образом когда мы кликнули на кнопке Click1
* мы кликнули на <strong>1</strong>
* мы кликнули на <h2>Event</h2>
* мы кликнули на <div class="wrapper">
* мы кликнули на <body>
* мы кликнули на <head>
* мы кликнули на <html>
* и на объекте window где мы поймали в лог событие click
* т.е. наши события передаются выше по всей цепочке родительских элементов
* и срабатывают на каждом родительском элементе
* и получается что наши события всплывают
* от самого внутреннего элемента на который мы кликнули
* и вверх
* и соответственно если обработчик события весит на глобальном объекте
* window - то он поймает все клики нашей страницы
*
* Если мы не хотим передавать событие от элемента на который мы кликнули
* выше то есть специальный метод который называется .stopPropagation()
* т.е. испльзуя stopPropagation мы как бы говорим всем внешним
* элементам включая объект window что это событие оно только для кнопки
* в данном случае
* Так выглядит ФАЗА ВСПЛЫТИЯ
*
* Фаза погружения идет наоборот сверху вниз
* Можем ли мы отловить событие которое происходит на стаднии погружения
* Как мы можем узнать об этом?
* Заходим в документацию addEventListener
* находим объект options
* вставляем его например в window чтобы ниже прослушивание событий не
* опускалось
* {capture: true}
* и в теле функции объекта window вставляет то же что и при погружении
*  event.stopPropagation();
*
* В общем виде будем выглядеть как в примере ниже
* Иными словами событие будет срабатывать только на объекте window
* но дальше вниз по дереву DOM элементов отрабатывать не будет.
*
* */
const handleClick1 = (event) => {
    //console.log(parseInt(event.target.dataset.number))  // 1
    //console.log(event.target.textContent)               // Click 1
    console.log("target >", event.target)                 // target > <button data-number="1" class="btn">Click1</button>
    console.log("currentTarget >", event.currentTarget)   // currentTarget > <button data-number="1" class="btn">Click1</button>
    console.log(event.target === event.currentTarget)     // false
    event.stopPropagation()                               // Window click! выводиться не будет
}
buttons.forEach(button => { button.addEventListener('click', handleClick1) })

window.addEventListener('click', function (event) {
    console.log('Window click!', event.target)
    event.stopPropagation();
}, {capture: true});

/*
* Внутри нашего коллбека значение this это event.currentTarget
* Если тот же пример но функция стрелочная то this не будет отрабатывать как
* event.currentTarget - т.е. мы теряем значение this
* Почему?
* Потому что стрелочная функция получает внешний по отношению к себе
* контекст и поэтому у нас остается объект event.currentTarget
* который мы можем продолжать использовать
*
* */
const img = document.querySelector('img');
img.addEventListener('mouseover', function (event){
    console.log(event.currentTarget);
    console.log(this);
})




















































