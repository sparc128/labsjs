/*
* ES6
*
* Лабораторная работа #10.
* Название: bind, call, apply, this.
* Цели:
* 1. Научиться Использовать методы
*    bind, call, apply, this. языка JavaScript
* 2. Научиться работать с методами
*    bind, call, apply, this. языка JavaScript
*    в консоли браузера
* 2. Понять метод
* 3. Понять метод
* 3. Понять метод
* 4. Понять метод
* 5. Понять метод
* 6. Сформулировать выводы, записать exel файлом, прислать в лк и почту.
* Ход работы:
* 1. Открыть codepen.io
* 2. Ввести листинг кода представленный ниже.
* 3. Получить результаты
* 4. Сформулировать, записать выводы
* 5. Прислать на проверку в лк, и почту.
*    * Данные вопросы задают на собеседованиях.
* Отчет:
* 1. Написать цели работы
* 2. Написать что студент делал в ходе выполнения работы своими словами
* 3. Написать вывод о проделанной работе
* 4. *Если работа содержит листинг кода - написать листинг кода
* 5. *Если работа содержит рисунки - добавить рисунки в отчёт
* 6. *Если работа содержит вывод в Console браузера, Elements, Sources,
*     Вывод информации в окно браузера иными словами любые результаты
*     работы стандарта html5** то их надо вносить в отчет в свободной форме
*     **html5 - стандарт вклюает в себя html4 и ниже, CSS3 и ниже, ES6 и ниже
* bind, call, apply, this.
* */

// Пример 1
const auto = {
    brand: "BMW",
    drive() {
        console.log(this)
        return `Заведем наш ${this.brand}`;
    }
}
// слева от точки .auto у нас находится объект к которому привязан
// контекст метода drive()
// это также относится к классам и к прототипам

// мы пожем использовать наш Drive за пределами объекта auto
const autoDrive1 = auto.drive;
// console.log(autoDrive1);

// bind - используется чтобы принудительно привязаться контекст
// нашей функции к объекту
// т.е. мы можем менять значение this внутри нашей функции

// мы как бы говорим js - создай новую функцию auto.drive и когда она
// будет вызываться значение this внутри этой функции будет равняться тому
// что я передаю внутрь метода bind
// и в нашем случае я передаю объект auto
const autoDrive = auto.drive.bind(auto);


// зачем это нужно? - сам метод bind зачем нужен?
// например мы можем использовать метод внутри какого то объекта
// уже с какими то другими данными
// * свойство brand11 должно быть одинаковым у auto11 и motorBike
// ** при вызове motorDrive будет вызвана motorBike и значение this
// будет равно "Suzuki"
const auto11 = {
    brand11: "BMW",
    drive11() {
        console.log(this)
        return `Заведем наш ${this.brand11}`;
    }
}

const motorBike = {
    brand11: "Suzuki"
}

// Пример 2
// создаем новую функцию motorDrive
const motorDrive = auto11.drive11.bind(motorBike);
// console.log(motorDrive);
// *** Вывод - У нас есть функция и для каждой функции в js
// доступен метод bind и
// внутрь этого метода можно передать
// объект и значение this внутри этой функции поменяется или привяжется
// к этому нужному объекту
// и мы можем привязать любой объект - например вместо motorBike
// указать { brand11: 'Корабль'}
const bikeDrive = auto11.drive11.bind({ brand11: "Корабль"});
console.log(bikeDrive());



// Пример 3
// Допустим мы устали каждый раз писать document.querySelector
// для выделения элементов в DOM дереве и хотим создать собственный
// упрощенный метод

// внутри $ есть функция и мы отобрали у этой функции привязку к
// объекту document вот этой строчкой -
// console.log($ === document.querySelector;
// т.е. нужная функция ни к чему не привязана
// т.е. если querySelector мы вызываем с привязкой к объекту document
// то знак доллара $ ни к чему не привязан
// это неправильно и надо исправить -
// чтобы все заработало указаываем метод .bind
// и внутрь bind мы передадим объект document - .bind(document)
const $ = document.querySelector.bind(document);
// таким образом мы сможем передавать в $ любой тег для его вызова
const header = $('h2') // document.querySelector('h2')
// console.log(header)


// Пример 4
const bill = {
    tip: 0.1,
    calculate(total) {
        console.log(this)
        return total + total * this.tip;
    },
    detail(dish1, dish2, sum) {
        return `Ваш обед (${dish1}, ${dish2}) стоит ${this.calculate(sum)} руб.`;
    }
}

//   const pay = bill.calculate(1000);
//   console.log(pay)
const pay = bill.detail('pizza', 'salad', 1000);
// мы можем воспользоваться функцией calculate за пределами объекта bill
const payCountOne = bill.calculate.bind(bill);
// console.log(payCountOne(1000));

// тут this равняется новому значению
const payCountTwo = bill.calculate.bind({tip: 0.2});
// calculate(total) - мы передаем общую сумму нашего аргумента
// 1000 в переменную total функции calculate
// console.log(payCountTwo(1000));

// и мы можем 1000 передавать внутрь метода bind
// в качестве второго аргумента
const payCountThree = bill.calculate.bind({tip: 0.2}, 1000);
// console.log(payCountThree());

// вопрос на собеседовании
const payCountFour = bill.calculate.bind(bill).bind({tip: 0.2}).bind({tip: 0.5});
// контекст привязывается один раз и мы не можем его повторно
// перепривязывать по цепочке
// поэтому и результат получится такой же как и тут
// const payCountThree = bill.calculate.bind({tip: 0.2}, 1000);
// console.log(payCountFour(1000));

// call и apply выполняют тоже что и bind но помимо этого будут
// запускать автоматическую функцию
const payFF = bill.calculate.bind(bill);
// аргументы в call передаем не в качестве аргументов новой функции
// а в качестве аргументов в метод call
const payCountFF = bill.calculate.call(bill, 1000);
console.log(payFF(1000));
console.log(payCountFF);

// apply - делает то же что и call только аргументы в этот метод
// передаются в качестве массива
const payCountApply = bill.calculate.apply(bill, [1000]);
console.log(payCountApply);

// польза apply не совсем очевидна потому что есть метод spread ...
// с помощью метода spread можно добавлять значения массива в качестве
// аргументов внутрь метода .call()
const bill1 = {
    tip: 0.1,
    calculate(total) {
        console.log(this)
        return total + total * this.tip;
    },
    // добавим объект detail c аргументами dish1, dish2, sum
    detail(dish1, dish2, sum) {
        return `Ваш обед (${dish1}, ${dish2}) стоит ${this.calculate(sum)} руб.`;
    }
}
// первый метод
const pay1 = bill1.detail('pizza','salad', 1000);
// console.log(pay1);

// второй метод пример с .call
const payCount33 = bill1.detail.call(bill, 'pizza','salad', 1000);
// console.log(payCount33);

// третий метод пример с .apply
const payCount55 = bill1.detail.apply(bill, ['pizza', 'salad', 1000]);
// console.log(payCount55);

// четвертый метод пример к .call с оператором spread
const payCount77 = bill1.detail.call(bill1, ...['pizza', 'salad', 1000]);
console.log(payCount77);













