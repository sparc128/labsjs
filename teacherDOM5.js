/*
* ES6
*
* Лабораторная работа #5.
* Название: DOM.
* Цели:
* 1. Определение DOM
* 2. Создавать и удалять элменты на странице
* 3. Динамически создать HTML разметку
* 3. Добавлять классы к элементам
* 4. Использовать обработчики событий на созданных элементах
* 5. Сформулировать выводы, записать exel файлом, прислать в лк и почту.
* Ход работы:
* 1. Открыть codepen.io
* 2. Ввести листинг кода представленный ниже.
* 3. Получить результаты
* 4. Сформулировать, записать выводы
* 5. Прислать на проверку в лк, и почту.
*    * Данные вопросы задают на собеседованиях.
* Отчет:
* 1. Написать цели работы
* 2. Написать что студент делал в ходе выполнения работы своими словами
* 3. Написать вывод о проделанной работе
* 4. *Если работа содержит листинг кода - написать листинг кода
* 5. *Если работа содержит рисунки - добавить рисунки в отчёт
* 6. *Если работа содержит вывод в Console браузера, Elements, Sources,
*     Вывод информации в окно браузера иными словами любые результаты
*     работы стандарта html5** то их надо вносить в отчет в свободной форме
*     **html5 - стандарт вклюает в себя html4 и ниже, CSS3 и ниже, ES6 и ниже
* DOM
* */

/*
* windows - глобальный объект внутри нашего браузера
* document - зона ответственности от открывающего тега <html>
  до закрывающего тега </html>
  *
  *
  *
  * 
*
* */
// создать div
const div = document.createElement('div');
// добавить к нему класс wrapper1
div.classList.add('wrapper1');
// поместить его внутри тега body
const body = document.body;
body.appendChild(div);
// создать заголовок H1 "DOM (Document Object Model)"
const header = document.createElement('h1');
header.textContent = 'DOM (Document Object Model)';
// добавить H1 перед DIV с классом wrapper1
div.insertAdjacentElement('beforebegin', header)
// создать список <ul></ul>
// добавить в него 3 элемента с текстом "один ,два,три"
const ul = `
    <ul>
        <li>Один</li>
        <li>Два</li>
        <li>Три</li>
    </ul></ul    
`;

// поместить список внутрь элемента с классом wrapper
div.innerHTML = ul;



// Cоздать изображение
const img1 = document.createElement('img');
// добавить следующие свойства к изображению
// 1. добавить атрибут source
img1.src = 'https://picsum.photos/240';
// 2. добавить атрибут width со значением 240
img1.width = 240;
// 3. добавить класс super
img1.classList.add('super');
// 4. добавить свойство alt со значением "Super Man"
img1.alt = 'Super man';
// 5. Поместить изображение внутрь элемента с классом wrapper1
div.appendChild(img1);
// 6. Используя HTML строку, создать DIV с классом 'pDIV'+c 2 параграфами
console.log(img1);
const elemHTML = `
<div class="pDIV">
    <p>Параграф 1</p>
    <p>Параграф 2</p>
</div>
`
// 7. Поместить этот DIV до элемента <ul></ul>
const ulList = div.querySelector('ul');
ulList.insertAdjacentHTML('beforebegin', elemHTML);
// 8. Добавить для 2-го параграфа класс text
const pDiv = document.querySelector('.pDIV');
// тут смотрим нащу HTML коллекцию чтобы подцепиться ко второму параграфу 'p'
console.log(pDiv.children);
// находим номер нашего абзаца в коллекции и цепляемся к нему
// а так же добавляем ко второму параграфу класс .text
pDiv.children[1].classList.add('text');
// 9. Удалить 1 параграф
pDiv.firstElementChild.remove();
// 10. Создать функцию generateAutoCard,
//     которая принимает 3 аргумента: brand, color, year
const generateAutoCard = (brand, color, year) => {
    // получаем текущий год
    const curDate = new Date();
    const curYear = curDate.getFullYear();
    // от текущего года отнимаем год выпуска машины ${curYear - year}
    return `
          <div class="autoCard">
             <h2>${brand.toUpperCase()} ${year}</h2>
             <p>Автомобиль ${brand.toUpperCase()} - ${year} года. Возвраста авто - ${curYear - year} лет.</p>
             <p>Цвет: ${color} </p>
             <button class='btn3' type='button'>Удалить</button>
          </div>
    `;
}
// Функция должна возвращать разметку HTML:
/*
  <div class="autoCard">
    <h2>BRAND YEAR</h2>
    <p>Автомобиль BRAND - YEAR года. Возвраста авто - YEARS лет.</p>
  </div>
* */

// Создать новый DIV с классом autos
const carsDiv = document.createElement('div');
carsDiv.classList.add('autos');
// Создать 3 карточки авто, используя функцию generateAutoCard
const carsList = [
    {brand: 'Tesla', year: 2015, color: 'red'},
    {brand: 'Lexus', year: 2016, color: 'silver'},
    {brand: 'Nissan', year: 2012, color: 'black'},
]

// получаем значения всех свойств в массиве carsList при помощи метода .map
// const carsHTML = carsList.map(car => {
//     return generateAutoCard(car.brand, car.color, car.year);
// })
// Проверяем себя - чтобы мы вывели таким образом в консоль
// console.log(carsHTML);
// видим что сумбур и нужен разделитель - метод join
// таким образом все это преобразуем в одну строку и получим
// разметку элементов для нашей карточки
const carsHTML1 = carsList.map(car => {
    return generateAutoCard(car.brand, car.color, car.year);
}).join('');

// Поместить эти карточки внутрь DIV с классом autos
carsDiv.innerHTML = carsHTML1;
// Выводим переменную carsDiv, чтобы убедиться что все работает
// console.log(carsDiv);
// Поместить DIV с классом autos на страницу DOM - до DIV с классом
// если на данном шаге все работает то добавляем переменные
// для нашей разметки
div.insertAdjacentElement('beforebegin', carsDiv);
// Добавить кнопку Удалить на каждую карточку авто

// При клике на кнопку  - удалять карточку из структуры DOM
// 1. Выбрать все кнопки
const buttons3 = document.querySelectorAll('.btn3');
//console.log(buttons3);
// 2. Создать функцию удаления
function handleClick(e) {
    const currentButton = e.currentTarget;
    // используя currentButton надо подобраться к головному div
    // нашей карточки - можно 2 способами сделать
    // 1 способ
    // currentButton.parentElement.remove();
    //console.log(currentButton.parentElement);
    // 2 способ - чтобы удалить карточку надо удалить головной класс
    currentButton.closest('.autoCard').remove();
}
// 3. Использовать цикл - чтобы повесить обработчик события на каждую кнопку
buttons3.forEach(button11 => {
    button11.addEventListener('click', handleClick);
});






































