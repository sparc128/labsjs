/*
* ES6
*
* Лабораторная работа #7.
* Название: localStorage.
* Цели:
* 1. Научиться Использовать localStorage
* 2. Дать определение localStorage
* 3. Понять методы get и set при работе со свойством localStorage
* 4. Сформулировать выводы, записать exel файлом, прислать в лк и почту.
* Ход работы:
* 1. Открыть codepen.io
* 2. Ввести листинг кода представленный ниже.
* 3. Получить результаты
* 4. Сформулировать, записать выводы
* 5. Прислать на проверку в лк, и почту.
*    * Данные вопросы задают на собеседованиях.
* Отчет:
* 1. Написать цели работы
* 2. Написать что студент делал в ходе выполнения работы своими словами
* 3. Написать вывод о проделанной работе
* 4. *Если работа содержит листинг кода - написать листинг кода
* 5. *Если работа содержит рисунки - добавить рисунки в отчёт
* 6. *Если работа содержит вывод в Console браузера, Elements, Sources,
*     Вывод информации в окно браузера иными словами любые результаты
*     работы стандарта html5** то их надо вносить в отчет в свободной форме
*     **html5 - стандарт вклюает в себя html4 и ниже, CSS3 и ниже, ES6 и ниже
* LocalStorage.
* */

// LocalStorage -  создадим форму полем ввода и кнопкой
// Механика: при добавлении нового элемента - он появляется в списке
// но если мы перезагрузим страницу то добавленные элементы в списке
// никуда не исчезнут
const addItemsForm = document.querySelector('.add-items-form');
const itemsList = document.querySelector('.items-list');
const items = JSON.parse(localStorage.getItem('items')) || []; // ** (3)

function addItem(e) {
    // отменим стандартное поведение - введенный текст в форме пропадает
    e.preventDefault();
    // получаем элемент формы - это сама форма
    //console.log(e.target);
    // получаем значения поля input когда мы вбиваем туда название ингридиента
    const text = e.target.item.value;
    console.log(text);
    // так будет выглядеть каждый ингридиент который мы добавляем
    const item = {
        text,
        checked: false
    }
    // при каждом добавлении ингридиента будем добавлять его в массив items
    items.push(item);
    //console.log(item);
    // то как выглядит наш новый массив после добавления нового элемента
    //console.log(items);
    // добавим вывод значения this - а это значение сама наша форма
    // так происходит потому что мы вешаем обработчик события на
    // нашу форму - это переменная addItemsForm - поэтому мы можем
    // использовать значение this в котором находится наша форма
    // и использовать метод reset(), чтобы перезагружать форму после
    // каждого добавления нового ингридиента
    // т.е. каждый раз когда мы добавляем новый ингридиент у нас
    // очищается наша форма, срабатывает метод reset и мы получаем
    // массив который заполняется нащими ингридиентами
    //console.log(this);

    // ** (1)
    // чтобы записать что то в localstorage мы используем метод
    // setItem() и внутри мы указываем ключ того объекта
    // который хотим записать назовем этот ключ items
    // второе указываем содержимое - в нашем случае это
    // массив items
    // т.к. в массиве все происходит в строчном формате
    // то используем метод stringify, чтобы перевести
    // массив items в строчный формат
    localStorage.setItem('items', JSON.stringify(items));

    // вызываем displayItems() каждый раз когда добавляем новый ингридиент
    // внутри будем передавать обновленный массив items после
    // каждого добавления нового ингридиента
    // и переменную itemsList куда мы хотим все это отображать
    // (рендерить)
    displayItems(items, itemsList);

    this.reset();
    // Проверка!
    //console.log(items);
    // Отлично - теперь мы получаем массив наших элементов items
    // и теперь займемся отображением наших элементов (ингридиентов
    // на нашей странице)
}

/*
* ingredients - все ингридиенты - список - массив
* ingredientsList - это тег ul внутри которого будем отображать ингридиенты
* */
function displayItems(ingredients, ingredientsList) {
    console.log(ingredients, ingredientsList);
    // возьмем список наших ингридиентов
    // т.е. перменную ingredientsList т.е. тег ul
    // и создадим для нее html - .innerHTML
    // мы возбмём все наши ингридиенты - переменная ingredients -
    // используем метод map чтобы пробежаться по каждому
    // ингридиенту т.е. мы будем получать каждый отдельный
    // ингридиент и его индексное значение
    // и создадим html - <li> ${ingredient.text} </li>
    // тут .text - это просто текст внутри каждого ингридиента
    // и избавимся от разделитя запятая методом .join('')
    // * внутри нашего innerHTML мы будем рендерить
    // строчные выражения, а не массив
    // усложним структуру потому что напротив каждого
    // ингридиента у нас есть checkbox
    // сверху будет поле <input /> - это будет каждый отдельный checkbox
    // и название каждого отдельного ингридиента мы заключим в тег
    // label
    // data-index будет равен индексному значению каждого элемента
    // и для каждого checkbox если мы его отмечаем к этому checkbox -
    // к тегу input должен добавляться атрибут checked
    // поэтому мы создадим условие - если свойство checked = true
    // то мы будем добавлять свойство checked в противном случае
    // будет добавляться пустая строка
    // и теперь label для каждого элемента свяжем с конкретным checkbox
    // для этого укажем атрибут for со значением
    // * data-index - все что с приставкой data мы получаем в виде объекта
    ingredientsList.innerHTML = ingredients.map((ingredient, index) => {
        return ` <li> 
                <input type='checkbox' id='item${index}' data-index='${index}' ${ingredient.checked ? 'checked' : ''}/>
                <label for='item${index}'> ${ingredient.text} </label> 
                </li> `;
    }).join('');
}

function toggleClick (e) {
    // отметаем - фильтруем все теги кроме input - т.е. нас не интересует
    // ничего кроме тегов input
    if (!e.target.matches('input')) return;
    // проверяем получение индекса из data-index
    console.log(e.target.dataset.index);
    // получаем id каждого конкретного элемента -
    // т.е. мы кликаем на элемент и мы получаем его конкретное индексное значение
    const element = e.target.dataset.index;
    // берем массив items и индексное значение элемента на который кликнули
    // и берем его свойство checked - items[element].checked
    // и при клике меняем его не противоположное -
    // для этого просто ставим восклицательный знак !items[element].checked
    //
    items[element].checked = !items[element].checked;

    localStorage.setItem('items', JSON.stringify(items)); // ** (3)

    // теперь получается что у нас обновляется массив items
    // т.е. его свойство checked поменялось с false на true
    // и нам нужно повторно показать (отрендерить)
    // обновленный массив items - items[element]
    // для этого мы копируем функцию displayItems(items, itemsList);
    // и для этого передаем items и обновленный itemsList
    displayItems(items, itemsList);
}

addItemsForm.addEventListener('submit', addItem);

/*
* Теперь мы должны к конкретному атрибуту добавлять атрибут checked
* Как это сделать?
* Мы должны выбрать тег ul с нашим списком который присвоен переменной
* items-list - и повесить на него обработчик событий
* */
console.log(items); // null // ** (4)
itemsList.addEventListener('click', toggleClick);
displayItems (items, itemsList); // ** (2)
/*
* LocalStorage определение.
* Свойство localStorage позволяет получить доступ к Storage объекту.
* localStorage аналогично свойству sessionStorage.
* Разница только в том, что свойство sessionStorage хранит данные в
* течение сеанса (до закрытия браузера), в отличие от данных,
* находящихся в свойстве localStorage, которые не имеют ограничений по
* времени хранения и могут быть удалены только с помощью JavaScript.
*
* Как его посмотреть в браузере?
* Есть вкладка Application - кликаем на нее мышкой
* Находим раздел LocalStorage - кликаем на него мышкой
* Из выпадающего списка выбираем наш локальный адрес
* Справа видим список объектов - в данном случае один объект
* Это наш массив в строчном формате
* Который содержит Key - ключ и Value - значение
* */

/*
* При каждом добавлении нового ингридиента в нашу форму мы будем
* записывать обновленный массив items в LocalStorage
*
* Для этого перейдем в функцию addItem(e) и сделаем запись
* после строчки
* items.push(item); ** (1)
*
* получим localStorage.setItem('items', JSON.stringify(items));
*
* теперь если мы перезагружаем нашу страницу -
* мы все сохраняем в нашем localstorag'e, но
* мы это не подтягиваем при перезагрузке нашей страницы
* Как это сделать ?
* Мы видим что у нас есть массив items = []; , который мы получаем
* при старте страницы и он пустой поэтому мы так же используем localstorage
* и используем не setItem(), а getItem() потому что мы хотим получить
* что то из localstorag'а и мы указываем название того что мы хотим
* получить - это у нас ключ 'items' и
* const items - это массив, а то что мы получаем из localstorag'а
* это будет в строчном формате поэтому это нам нужно будет
* парсить - переводить обратно из строчного формата в массив JSON.parse()
* const items = JSON.parse(localStorage.getItem('items'));
* Но пока наш массив items не будет отображаться потому что мы при старте
* не используем нашу функцию displayItems(ingredients, ingredientsList)
* скопируем ее и будет вызывать ее при каждой перезагрузке страницы -
* displayItems ()
* и передадим в нее массив items уже заполненный
* и itemsList это наша переменная с тегом ul - куда мы все будем рендерить
* displayItems (items, itemsList); // ** (2)
*
* */

/*
* След момент - если мы отмечаем любой элемент списка и
* перезагружаем страницу то отметка на checkbox
* никак не сохраняется
* Происходит это потому что в функции toggleClick(e) {}
* где мы меняем свойство каждого отмечаемого элемента -
* мы обновленный массив items не записываем в localstorage
* исправим это - передадим сюда массив в строчном формате
* localStorage.setItem('items', JSON.stringify(items)); // ** (3)
*
* */

/*
* Следующая проблема:
* Что если на старте наш localstorage пустой?
* Удаляем содержимое localstorage
* Перезагружаем браузер и получаем ошибку -
* мы вызываем функцию displayItems (items, itemsList); // ** (2)
* но не передаем в нее наши ингридиенты
*
* можем проверить что мы туда передаем на текущий момент времени
* console.log(items) // ** (4)
* вернет null
* поэтому, чтобы этот момент решить нам надо на старте сказать что
* если в localstorage что то есть то мы оттуда это берем, парсим
* и записываем в переменную items, а если ничего нету то мы
* используем оператор || (или) items у нас будет пустым массивом
* const items = JSON.parse(localStorage.getItem('items')); // ** (3)
* таким образом получим
*  const items = JSON.parse(localStorage.getItem('items')) || []; // ** (3)
* */

/*
* Чем полезен localstorage?
* Он позвляет почти без использования backend записывать какие либо
* данные в браузер пользователя и хранить их там.
* */





















