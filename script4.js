/*
*  Массивы
* */

// Создание массива
let arr = new Array(); // используется редко
let arr2 = [];

// Заполнение значений массива
let arrOne = [
    'Ваня',
    'Binfy',
    'Jordan', // висячая запятая
];
// или
let arrTwo = ['123','456','789',];

// Различные типы значений
let arrThree = [
    "Jordan",
    {
      type: "JS",
      age: 36
    },
    true,
    function () {
        console.log('Hi');
    },
];

// многомерные массивы
let matrix = [
    [1,2,3],
    [4,5,6],
    [7,8,9],
];

// Получение значений
let one1 = [
    '1234', // 0 позиция
    '5678', // 1 позиция
    '9123', // 2 позиция
];
console.log(one1[1]);
console.log(one1[5]);

// Вызов элементов массива
let arrThree1 = [
    "Jordan",
    {
        type: "JS",
        age: 36
    },
    true,
    function () {
        console.log('Hi');
    },
];
console.log(arrThree1);
console.log(arrThree1[0]);
console.log(arrThree1[1].type);
console.log(arrThree1[2]);
arrThree1[3](); // вызов функции как элемента входящего в массив, 3-позиция элемента в массиве

// Получение конкретного значения из многомерного массива
let matrix1 = [
    [1,2,3],
    [4,5,6],
    [7,8,9],
];
console.log(matrix1);
console.log(matrix1[0][1]); // 0 - номер строки, 1 - ключ - позиция в этом элементе

// Длина массива - свойство length
let arrOne1 = [
    'Ваня',
    'Binfy',
    'Jordan', // висячая запятая
];
console.log(arrOne1);
console.log(arrOne1.length);

// Доступ к массиву
/*
* Напомню что массив является объектом и следовательно
*  ведет себя как объект - например копируется по ссылке
*
* */
let arrOne2 = [
    'Ваня',
    'Binfy',
    'Jordan', // висячая запятая
];
console.log(arrOne2);
let arrNew = arrOne2;
arrNew.length = 2;
console.log(arrOne2);

// Изменение значений
let arrOne3 = ['Ваня', 'Binfy', 'Jordan'];
// Меняем существующие
arrOne3[0] = 'Vasiya';
console.log(arrOne3);
// Добавляем новое
arrOne3 = 'IVAN';
console.log(arrOne3);

/*
*  Методы массивов
*
*  Перед этим надо поговорить о вариантах применения массивов
*
*  Первый из них это очередь или упорядоченная коллекция элементов
*  Очередь поддерживает два вида операций:
*  1. добавление элемента в конец очереди
*  2. удаление элемента вначале - сдвигая очередь,
*     так что второй элемент становится первым
*
*  Другой вариант применения для массивов - структура данных,
*  называемая стек.
*
*  Стек поддерживает два вида операций:
*  1. Добавление элемента в конец
*  2. Удаление последнего элемента
*
*  Массивы в js могут работать и как очередь и как стек.
*  Мы можем добавлять/удалять элементы как в начало, так в конец массива.
*  И в этом нам помогут следующие методы:
* */


// Метод push - добавляет элемент в конец массива
let arr5 = ['Ваня', 'Binfy', 'Jordan'];
arr5.push('asd');
console.log(arr5);
arr5.push('west', 'usa');
console.log(arr5);

// Метод shift - удаляет элемент в самом начале
//  так что второй элемент становится первым
let arr6 = ['Ваня', 'Binfy', 'Jordan'];
arr6.shift();
console.log(arr6);

// Метод pop - удаляет последний элемент в массиве
let arr7 = ['Ваня', 'Binfy', 'Jordan'];
arr7.pop();
console.log(arr7);

// Метод unshift - добавляет элементы в начало массива тем самым
// сдвигая все остальные
let arr8 = ['Ваня', 'Binfy', 'Jordan'];
arr8.unshift('ANNA');
console.log(arr8);

/*
*  Методы push/pop выполняются быстро, а методы shift/unshift - медленно.
*
*  Давайте вначале рассмотрим на примере добавление элемента
*  в начало массива (unshift);
*
*  Просто взять и добавить элемент с номером 0 недостаточно.
*  Нужно также заново пронумеровать остальные элементы.
*
*  Операция unshift должна выполнить 3 действия:
*  1. Добавить элемент с индексом 0.
*  2. Сдвинуть все элементы вправо, заново пронумеровать их,
*  заменив 0 на 1 , 1 на 2, и т.д.
*  3. Обновить свойство length.
*
*  Чем больше элементов содержит массив тем больше временти
*  требуется для того, чтобы их переместить,
*  больше операций с памятью.
*
*  А вот чтобы добавить элемент в конец массива (метод push)
*  не нужно ничего перемещать.
*  Так же не нужно заново нумеровать элементы.
*  Достаточно увеличить значение length.
* */

// Удаление /добавление/изменение конкретных элементов
let arrm = ['qaz','wsx','edc'];
delete arrm[1];
console.log(arrm);
console.log(arrm[1]);
console.log(arrm.length);


// метод splice - позволяет добавлять, удалять и заменять элементы.
// синтаксис arr.splice(index[, deleteCount, elem1, ..., elemN])

// Удаляем элемент
let ar1 = ['qaz','wsx','edc'];
// Начиная с первой позиции удаляем один элемент - 'wsx'
ar1.splice(1,1);
console.log(ar1);

// Удаляем элемент и возвращаем его в переменную
let ar2 = ['qaz','wsx','edc'];
let removed = ar2.splice(1,1);
console.log(removed);

// Заменяем элементы
let ar3 = ['qaz','wsx','edc'];
//Начиная с нулевой позиции заменяем один элемент
ar3.splice(0,1, 'ZZZZZ');
console.log(ar3);

// Добавляем элементы
let ar4 = ['qaz','wsx','edc'];
// Начиная с первой позиции (перед 'wsx'), добавляем оба элемента
ar4.splice(1,0, 'Kolya', 'Masha');
console.log(ar4);

// Удаляем элемент
let ar5 = ['qaz','wsx','edc'];
// Начиная с последней позиции ('edc'), удаляем один элемент
ar5.splice(-1,1);
console.log(ar5);

/*
*  Метод slice.
*
*  Создает новый массив, в который копирует часть либо весь массив
*
*  Синтаксис arr.slice([start], [end]) Не включая [end]
* */
// Копируем часть массива.
let ar6 = ['one','two','three'];
// Начиная с первой позиции 'two' до второй позиции 'three' (не включая)
let ar67 = ar6.slice(1,2);
console.log(ar67);

// Начиная с предпоследней позиции two
// до последней three (не включая)
let ar68 = ar6.slice(-2,-1);
console.log(ar68);

// Копируем весь массив
let ar70 = ar6.slice();
console.log(ar70);

/*
* Метод concat.
* Создает новый массив, в который копирует данные из других массивов
* и дополнительные значения (в конец массива)
* Синтаксис arr.concat(arg1, arg2, ...)
*
* */
let r1 = ['res1','res2','res3'];
let r2 = r1.concat('res5');
console.log(r2);

/*
* Поиск в массиве
*
* Методы indexOf/lastIndexOf и includes
* аналоги строковым методам
* 1. arr.indexOf(item, from) ищет item, начиная с индекса from,
* и возвращает индекс, на котором был найден искомый элемент,
* в противном случае -1.
* 2. arr.lastIndexOf(item, from) - тоже самое, но ищет справа налево.
* 3. arr.includes(item, from) - ищет item, начиная с индекса from,
* и возвращает true если поиск успешен.
* */
let arr15 = ['tag1', 'tag2', 'tag3'];
// indexOf
console.log(arr15.indexOf('tag2'));
console.log(arr15.indexOf('tag5'));
console.log(arr15.includes('tag2', 2));
// includes
console.log(arr15.includes('tag2'));
console.log(arr15.includes('tag5'));
console.log(arr15.includes('tag2', 2));

/*
* find и findIndex
* поиск в массиве объектов с опеределенным условием
*
* Синтаксис
* let result = arr15.find(function(item, index, array) {
*  если true - возвращается текущий элемент и перебор прерывается
*  если false- если все итерации оказались ложными возвращается undefined
* });
* */
let arr22 = [
    {name: 'Anna', age: 35 },
    {name: 'Sabina', age: 20 },
    {name: 'Masha', age: 'Не скажу' },
]

let resultOne = arr22.find(function (item, index, array){
   return item.age === 20;
});
console.log(resultOne);

// тоже самое с использованием стрелочной функции
let resultOne1 = arr22.find(item => item.age === 35);
console.log(resultOne1);

// findIndex
let resultTwo = arr22.findIndex(item => item.age === 20);
console.log(resultTwo);

// filter
/*
* Метод ищет все элементы на которых функция колл-бэк вернёт true.
*
* let results = arr.filter(function (item, index, array) {
* если true  -  элемент добавляется к результату, и перебор продолжается
* false - возвращается пустой массив в случае если ничего не найдено
* })
* */
let arr88 = [
    {name: 'Marina', age: 36},
    {name: 'Yuliya', age: 33},
    {name: 'Anna',   age: 25},
]
let result55 = arr88.filter(function (item, index, array){
    return item.age >= 33;
});
console.log(result55);

// Сортировка массивов
// Метод sort(fn)
// сортирует массив на месте меняя в нем порядок элементов
// Сортировка слов
let ar12 = ['one', 'two', 'three'];
console.log(ar12.sort());

// сортировка чисел
let ar1212 = [8, 22, 1];
console.log(ar1212.sort());

/*
*  По умолчанию элементы сортируются как строки.
*  Для строк применяется лексикографический порядок.
*  И действительно выходит что "8" > "22"
* */
console.log("8" > "22");

// Функция которую мы написали сами для сортировки чисел
// сортировка чисел
let ar35 = [8, 22, 1];
console.log(ar35.sort());
// Функция сортировки
function compareNumeric(a,b) {
    console.log(`Сравниваем ${a} и ${b}`);
    if (a > b) return 1;
    if (a == b) return 0;
    if (a < b) return -1;
    // или вместо трех строк return a - b
}
console.log(ar35.sort(compareNumeric));

// Краткая сортировка чисел - в одну строку
// та же сортировка что и выше но без if
let ar36 = [10, 22, 1];
console.log(ar36.sort((a,b) => a - b));

// Метод reverse
// меняет порядок элементов в массиве на обратный.
let arr20 = ['one', 'two', 'three'];
console.log(arr20.reverse());

// Метод map.
// вызывает функцию для каждого элемента массива
// и возвращает массив результатов выполнения этой функции.
// let rezult = arr.map(function(item,index,array) {
//      // возвращается новое значение вместо элемента
// });
let arr23 = ['one', 'two', 'three'];
let rezult23 = arr23.map(function(item, index, array){
   return item[0];
});
console.log(arr23);
console.log(rezult23);
// тоже самое но с использованием стрелочной функции
let rezult888 = arr23.map(item => item[0]);
console.log(rezult888);

// Методы split и join
/*
*  Метод split преобразовывает строку в массив
*  по заданному разделителю
*  Синтаксис: str.split(delim)
* */
let str1 = 'Ваня,Оля,Аня';
let arr57 = str1.split(',');
console.log(arr57);

/*
* Можно ограничить количество объектов
* которые попадут в массив
* */
let arrTwo2 = str1.split(',', 2);
console.log(arrTwo2);

/*
* Метод join преобразовывет массив
* в строку с заданным разделителем
* Синтаксис: arr.join(glue)
* */
let arr123 = ['one', 'two', 'three'];
let str111 = arr123.join(' | ');
console.log(str111);

// Получение строки из массива
let arr233 = ['one', 'two', 'three'];
console.log(String(arr233));

/*
* проверка Array.isArray()
*
* Напомню, массивы не образуют отдельный тип данных.
*
*
* */
let obj1 = {};
let arr111 = [];
console.log(typeof obj1);
console.log(typeof arr111);
// Как же нам узнать где массив а где нет?
if (Array.isArray(arr111)) {
    console.log('Это массив!');
} else {
    console.log('Это не массив!');
}

// Перебор элементов
let arr1112 = ['Анна','Мирослава','Женя'];
console.log(arr1112);
// Цикл for - получаем и ключ и значение массива
for (let i=0; i < arr1112.length; i++) {
    console.log(arr1112[i]);
}
// Если нам достаточно получать только значения массива то
// мы можем использовать цикл for...of
for (let arrItem11 of arr1112) {
    console.log(arrItem11);
}

/*
* Перебор элементов
*
* Метод перебора forEach
*
* Выполняет функцию для каждого элемента массива
*
* */
let arrG = ['one', 'two', 'three'];
arrG.forEach(function (item, index, array) {
   console.log(`${item} находится на ${index} позиции в ${array}`);
});
// Стрелочная функция аналог
arrG.forEach((item, index, array) => {
    console.log(`${item} находится на ${index} позиции в ${array}`);
});
// В качестве параметра forEach можем указывать функцию
// и работать будет все так же
arrG.forEach(show1);
function show1 (item){
    console.log(item);
}

/*
* Методы reduce/reduceRight
*
* Итак мы выяснили что если нам нужно перебрать
* массив -мы можем использовать forEach, for, или for..of.
* Если нам нужно перебрать массив и вернуть данные для каждого элемента
* то мы используем map.
*
* Методы arr.reduce и arr.reduceRight похожи на эти методы,
* но они немного сложнее и используются для вычисления какого-нибудь
* единого значения на основе всего массива.
*
* */
//Синтаксис
// let value333 = arrG.reduce(function (previousValue, item, index, array){
//    //...
// }, [initial]);
/*
* К привычным нам аргументам item, index, array добавляется previousValue:
* previousValue - результат предыдущего вызова этой функции,
* равено initial при первом вызове (если передан initial),
* item - очередной элемент массива,
* index - его индекс,
* array - сам массив.
*
* Функция применяется по очереди ко всем элементам массива и "переносит"
* свой результат на следующий вызов.
*
* */
// Рассмотрим пример
let arrMas1 = [1,2,3,4];
let reduceValueOne = arrMas1.reduce(function (previousValue, item, index,array){
    return item + previousValue;
},0);
console.log(reduceValueOne);
/*
* Механика работы reduce
*
* Шаг 1
* previousValue = 0
* item = 1
* их сумма = 1
*
* Шаг 2
* previousValue = 1
* item = 2
* их сумма = 3
*
* Шаг 3
* previousValue = 3
* item = 3
* их сумма = 6
*
* Шаг 4
* previousValue = 6
* item = 4
* их сумма = 10
*
* */

let arr3535 = ['Аня', 'Маня', 'Даша'];
/*
* Если не указать начальное значение то оно будет равно
* первому элементу массива (previousValue=Аня)
*
* А работа метода начинается со второго элемента (item=Маня)
*
* */
let reduceValueTwo = arr3535.reduce(function (previousValue, item,index,array){
    console.log(previousValue);
    console.log(item);
    return `${item}, ${previousValue}`;
});
console.log(`Пользователи: ${reduceValueTwo}`);

/*
* Метод arr.reduceRight работает аналогично но проходит по массиву справа
* налево
*
* */

/*
* Массив используем как массив
*
* Следует помнить что массив является объектом и следовательно
* ведет себя как объект
* */
let arr1818 = ['Аня', 'Маня', 'Даша'];
console.log(typeof arr1818);
// Добавление нечислового свойства
arr1818.name = "Nadiya";
console.log(arr1818);
/*
* Но то что дествительно делает массивы особенными - это их внутреннее
* представление. Движок JS старается хранить элементы
* массива в непрерывной области памяти - один за другим.
* Существуют и другие способы оптимизации благодаря которым
* массивы работают очень быстро.
*
* Но все они утратят эффективность если мы перестанем работать
* с массивом как с упорядоченной коллекцией данных и начнем использовать
* его как обычный объект.
*
* Варианты неправильного применения массива:
* 1. Добавление нечислового свойства,например: qrr.test = 5.
* 2. Создание "дыр", например: добавление arr[0],
* затем arr[1000](между ними ничего нет).
* 3.Заполнение массива в обратном порядке, например arr[1000], arr[999]
* и т.д.
*
* Все это приведет с скорости снижения работы массивов.
* */

/*
* Домашка
*
* Задача №1.
* Какое число длину мы получим?
* let arr = ['Ваня','Оля','Аня']ж
* let newArr = arr;
* newArr.push('Петя');
* console.log(arr.length);
*
*
* Задача № 2.
* Создайте массив users с элементами "Ваня"  и  "Иштван".
* Добавьте "Оля" в конец.
* Замените значение в "Иштван" на "Петя".
* Ваш код для поиска значения должен
* работать для массиовов с любой длиной.
* Удалите первый элемент массива и покажите его.
* Вставьте "Маша" и "Паша" в начало массива.
*
*
* Задача № 3.
* Удалить элемент "Иштван" и возвратить его в переменную
* let arr = ['Ваня','Иштван','Оля'];
*
* Задача № 4.
* Сделать из строки массив
* let str = 'Ваня,Иштван,Оля';
*
*
* Задача № 5.
* Чему равен previousValue в начале работы метода?
* let arr = [9,2,8];
* let reduceValue = arrTwo.reduce(function(previousValue,item,index,array){
*  console.log(previousValue);
* });
*
*
* */

/*
 * 1 - DOM
 * 2 - События
 * 3 - Сделать десяток проектов на тему: Создание дерева DOM элементов
 * проекта на чистом JS
 * 4
 *
 *
 */

// Пример по пункту 3. DOM-дерево как структура проекта на чистом JS
const div = window.document.createElement('div');
const header = window.document.createElement('h1');
header.innerText = 'Learn JS';
header.classList.add('test__color'); // делаем заголовок красным
div.appendChild(header);
window.document.body.appendChild(div);
console.log(div);
console.log(header);
// Создадим атрибуты к нашему документу используя DOM
const a = window.document.createElement('a');
a.innerText = 'MDN JS';
a.href = 'https://developer.mozilla.org/ru/docs/Web/JavaScript';
div.appendChild(a);
console.log(a);
// Пример по пункт 4.
const aTag = window.document.querySelector('a');
console.log(aTag);
aTag.addEventListener('click', function (e){
    // Отменяем стандартное поведение ссылки
    e.preventDefault();
    // Указываем желаемый адрес перехода по нашей ссылке
    window.location = 'https://youtube.com';
});
// Добавим в HTML документ второй тег <p>
// Вопрос - как повесить обрабочик события и на первый который уже был
// тег <p> и на второй тег <p> - который мы только что создали
const pTags = window.document.querySelectorAll('p');
pTags.forEach(pTag => {
    pTag.addEventListener('click', function (){
        console.log('TEST!!!');
    });
});


/*
* Список основых тем для изучения JS
* 1.Манипулирование DOM-элементами    - DOM элементы
* 2.Изменение атрибутов DOM-элементов - Атрибуты DOM элементов
* 3.События в JavaScript'е            - JavaScript События
* 4.Знаки равно, сравнение, и т.д.    - Типы данных + Сравнения
* 5.Асинхронные запросы               - Асинхронные запросы
* 6.ES6 фичи                          - Нововведения ES6
* 7.Методы массивов
* */

/*
*  Типизация
* */
// При сравнении типов данных в JS
// Два знака ==   не учитываются
// Три знака ===  учитываются
// Пример:
console.log('"5" == 5 >', "5" == 5);    // true
console.log('"5" === 5 >', "5" === 5);  // false
/*
* Задачи:
* 1. 1.1 - Понять как работает сравнение на 2 знака равно     ==
*    1.2 - Понять как работает сравнение на 3 знака равно    ===
*    1.3 - В общем можно сказать 1.1, 1.2 - примитивные значения
* 2.     - Понять  КАК  работает сравнение объектов в JavaScript
* */

/*
* Чтобы добавить динамики в проект надо разобраться с внешними API
* Иными словами надо разобраться с асинхронностью
* Асинхронные запросы
*
* Что такое promise ?
* Есть бесплатные тестовые сервисы
* которые предоставляют тестовые API с данными к которым можно обращаться
* Например сервис Sample APIs
*
* */

/*
* Получение свойства объекта по ES6
* */
const car = {
    brand: 'bmw',
    year: 2015,
}
const { brand } = car
console.log(brand)

/*
* Оператор spread
* Обозначается тремя точками ...
* */
// Пример:
const cars1 = ['bmw','audi'];
const cars2 = ['volga','lada'];
const carsAll = [...cars1, ...cars2];
console.log(carsAll)
// Так же оператор spread используется для копирования объектов

/*
* Еще одна фишка ES6 это бектики - обратные кавычки ``
* Берем первый элемент и говорим что эта машина 2015 года
* Пример:
* */
console.log(`${carsAll[0]} - 2015 года`)

/*
* Изучить методы массивов map, sort, reduce, etc
* Начать лучше с методов обхода на MDN JS
* */




























